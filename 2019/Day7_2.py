from itertools import permutations

inp='3,8,1001,8,10,8,105,1,0,0,21,30,51,76,101,118,199,280,361,442,99999,3,9,102,5,9,9,4,9,99,3,9,102,4,9,9,1001,9,3,9,102,2,9,9,101,2,9,9,4,9,99,3,9,1002,9,3,9,1001,9,4,9,102,5,9,9,101,3,9,9,1002,9,3,9,4,9,99,3,9,101,5,9,9,102,4,9,9,1001,9,3,9,1002,9,2,9,101,4,9,9,4,9,99,3,9,1002,9,2,9,1001,9,3,9,102,5,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,99'

#inp='3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5'

#inp='3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10'

max_val=0


for phases in permutations([5,6,7,8,9],5):

	#phases = [9,8,7,6,5]
	
	regs=[[int(a) for a in inp.split(',')] for i in range(5)]

	pointers = [0,]*5

	signals = [0]
	power = 0
	firstTime = True
	done = False
	while True:
		regi=0
		for p in phases:
			if firstTime:
				type_input=[p,]+signals[:]
			else:
				type_input=signals[:]
			signals = []
			type_input_index=0
			
			#print(type_input,p,regi)

			reg = regs[regi][:]
			i=pointers[regi]
			while (True):
				cmd = reg[i]
				try:
					a = reg[i+1]
					if (cmd % 1000)//100 == 0:
						a=reg[a]
				except:
					a=0
				try:
					b = reg[i+2]
					if (cmd % 10000)//1000 == 0:
						b = reg[b]
				except:
					b = 0
				#print(reg[i:i+4],a,b)

				cmd = cmd % 100
				if cmd == 1:
					reg[reg[i+3]] = a + b
					i+=4
				elif cmd == 2:
					reg[reg[i+3]] = a * b
					i+=4
				elif cmd == 3:
					if type_input_index >= len(type_input):
						break
					reg[reg[i+1]]=type_input[type_input_index]
					type_input_index+=1
					i+=2
					
				elif cmd == 4:
					#print(a)
					signals.append(a)
					i+=2
				elif cmd  == 5:
					i +=3
					if a != 0:
						i = b
				elif cmd  == 6:
					i += 3
					if a == 0:
						i = b
				elif cmd  == 7:
					reg[reg[i+3]] = 1 if a < b else 0
					i += 4
				elif cmd  == 8:
					reg[reg[i+3]] = 1 if a == b else 0
					i += 4
				elif cmd==99:
					#print("Halt ",regi,i)
					done = True
					break
				else:
					print("Bad stuff happened",cmd, i,reg)
					break
		
			#print(reg,regi,i)
		
			#Stores registers for looping
			regs[regi]=reg[:]
			pointers[regi]=i
			regi+=1
			
			if done:
				i=1000000000
				#print("Ending: ",i,regi,power,signals)
				#break
			
			power = signals[-1]
		firstTime = False
		
#		print(pointers)
#		for r in regs:
#			print(r)
		
		if done:
			break
	
		

#	print(phases,power)
	if power>max_val:
		max_val=power
		print(phases,power)
	#quit()
print(max_val)

quit()