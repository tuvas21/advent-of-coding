fname='Day22.inp'

def deal_new_stack(index,ncards):
	return ncards-index-1
	
def cut(index,ncards,n):
	return (index + n + ncards) % ncards

def deal_increment(index,ncards,n):
	return ((ncards - n) + (- n)* (ncards - index-1)) % ncards
	
def o_deal_increment(cards,n):
	i=0
	ret = [0,]*len(cards)
	for c in range(len(cards)):
		ret[i]=cards[c]
		i = (i + n)%len(cards)
	return ret

ncards = 119315717514047
times = 101741582076661

cardIndex = 2020

instructions = []

#This encodes the functionality
offset = 0
increment = 1

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip()

		if dat.startswith('deal with increment'):
			increment *= pow(int(dat[20:]),ncards-2,ncards)
		elif dat.startswith('cut'):
			# The nth card is increment * n, thus this works.
			offset += int(dat[4:])* increment 
		elif dat=='deal into new stack':
			increment = -increment
			offset += increment
#Some fancy math here to do number of times
offset = offset * (1 - pow(increment,times,ncards))* pow(1-increment,ncards-2,ncards)
increment = pow(increment,times,ncards) 

print ((cardIndex * increment + offset)%ncards) 



