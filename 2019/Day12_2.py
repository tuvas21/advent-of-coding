fname='Day12.inp'

def mag(vector):
	return sum([abs(n) for n in vector])
	
def returned(start,current,v):
	return all([a[v]==b[v] for a,b in zip(start,current)])
	
location = []
velocity = []
y=0
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip().split(',')
		
		x=int(dat[0].split('=')[1])
		y=int(dat[1].split('=')[1])
		z=int(dat[2].split('=')[1][:-1])
		#print(x,y,z)
		
		location.append([x,y,z])
		velocity.append([0,0,0])
		
start_location = [v[:] for v in location[:]]
start_velocity = [v[:] for v in velocity[:]]
		
i=0
printed = [False, False, False]
while True:
	i += 1
	#print(i)
	for p in range(len(location)):
		for c in range(len(location)):
			for a in range(3):
				if location[p][a]>location[c][a]:
					velocity[p][a]-=1
				elif location[p][a]<location[c][a]:
					velocity[p][a]+=1
	for p in range(len(location)):
		for a in range(3):
			location[p][a]+=velocity[p][a]
		#print(location[p],velocity[p])
	for a in range(3):
		if returned(start_location,location,a) and returned(start_velocity,velocity,a) and not printed[a]:
			print(i,a)
			printed[a]=True
	if all(printed):
		break
				

print(i)