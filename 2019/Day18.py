fname='Day18.inp'

lower='abcdefghijklmnopqrstuvwxyz'
upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'

class PathBetweenKeys:
	def __init__(self,keysRequired,distance):
		self.keysRequired=keysRequired
		self.distance = distance
		

offsets=[(-1,0),(1,0),(0,1),(0,-1)]

map =[]
mapKeys=[]
mapDoors =[]
ty=0
objects = {}
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip()
		map.append([c for c in dat])
		for tx in range(len(dat)):
			if dat[tx]=='@' or dat[tx] in lower:
				objects[dat[tx]]=(tx,ty)
			if dat[tx] in lower:
				mapKeys.append(dat[tx])
			elif dat[tx] in upper:
				mapDoors.append(dat[tx])
		
		ty+=1
		
print(mapKeys)


#This will remove dead end branches. Small improvement, but...
pruned = 0
found = True
while found:
	found=False
	for cx in range(1,len(map[0])-1):
		for cy in range(1,len(map)-1):
			ch = map[cy][cx]
			if ch =='.' or ch in upper:
				numWalls = 0
				neighbors = ''
				for o in offsets:
					tx=cx+o[0]
					ty=cy+o[1]
					ch = map[ty][tx]
					neighbors+=ch
					if ch =='#':
						numWalls +=1
				#print(cx,cy,neighbors,numWalls)
				if numWalls ==3:
					map[cy][cx]='#'
					pruned+=1
					found=True
print("Pruned ",pruned)
for row in map:
	print(''.join(row))
	
pathDistance={}
requiredKeys={}
	
for obj in objects:
	tested=[]
	emptySpace=[objects[obj]]
	testedLocations = {objects[obj]:PathBetweenKeys('',0)}
	while len(emptySpace)>0:
		cx,cy=emptySpace.pop(0)
		data = testedLocations[(cx,cy)]
		for o in offsets:
			tx=cx+o[0]
			ty=cy+o[1]
			ch = map[ty][tx]
			if (tx,ty) in testedLocations:
				continue
			if ch != '#':
				emptySpace.append((tx,ty))
			if ch =='.' or ch in lower or ch =='@':
				testedLocations[tx,ty]=PathBetweenKeys(data.keysRequired,data.distance+1)
			elif ch in upper:
				testedLocations[tx,ty]=PathBetweenKeys(data.keysRequired+ch,data.distance+1)
				
			if ch in lower:
				pathDistance[(obj,ch)]=data.distance+1
				requiredKeys[(obj,ch)]=data.keysRequired

print("Total connections found:",len(pathDistance))

#This is based on the range I'm getting from testing of some possible solutions
minDistance = 3650

def function_loop(keys,cKey,distance):
	global minDistance
	combos = []
	if distance > minDistance:
		return []
	
	if len(keys)==len(mapKeys) and minDistance > distance:
		print(distance)
		minDistance = distance
		return[]
	
	for obj in objects:
		if obj not in keys and obj !='@' and obj != cKey:
			dist = distance+pathDistance[(cKey,obj)]
			if dist > minDistance:
				continue
			hasAllKeys=True
			for k in requiredKeys[(cKey,obj)]:
				if k.lower() not in keys:
					hasAllKeys=False
					break
			if hasAllKeys:
				combos.append((dist,obj,keys+obj))
	return combos
	
print(len(mapKeys))
#Kick start the process	
nCombos = []
for obj in objects:
	if obj != '@' and len(requiredKeys[('@',obj)])==0:
		nCombos.append((pathDistance[('@',obj)],obj,obj))
while len(nCombos)>0:
	level=len(nCombos[0][2])
	print("New Level ",len(nCombos),level)
	#This will filter the numbers to speed things up
	if len(nCombos)>1000000:
		mxv = max(nCombos)[0]
		mnv=min(nCombos)[0]
		limit = (mxv-mnv)*0.5+mnv
		combos = [c for c in nCombos if c[0]<=limit]
		print ("Filtered: New count is ",len(combos))
	else:
		combos = nCombos[:]
	if len(combos)==0:
		break
	print(combos[0])
	print(combos[-1])
	nCombos = []
	for c in combos:
		ret = function_loop(c[2],c[1],c[0])
		nCombos += ret
		#print(len(nCombos))
		#print(ret)
	#break
	
print(minDistance)
	
			