fname='Day20.inp'


offsets=[(-1,0),(1,0),(0,1),(0,-1)]

map =[]
portals = {}
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		map.append([c for c in content[:-1]])

for y in range(len(map)):
	for x in range(len(map[0])):
		ch = map[y][x]
		portal = ''
		if ch not in ' #.':
			#print(ch,x,y)
			#Plus one when moving to the outer, -1 when moving to the inner. Changes the levels.
			if y==0:
				portal = ch + map[y+1][x]
				point = (x,y+2,1)
			elif x==0:
				portal = ch + map[y][x+1]
				point = (x+2,y,1)
			elif y==len(map)-1:
				portal = map[y-1][x]+ch
				point = (x,y-2,1)
			elif x==len(map[0])-1:
				portal = map[y][x-1]+ch
				point = (x-2,y,1)
			elif x>2 and y>2 and x<len(map[0])-2 and y<len(map)-2:
				if map[y][x-1]=='.':
					point = (x-1,y,-1)
					portal = ch + map[y][x+1]
				elif map[y-1][x]=='.':
					point = (x,y-1,-1)
					portal = ch + map[y+1][x]
				elif map[y][x+1]=='.':
					point = (x+1,y,-1)
					portal = map[y][x-1] + ch
				elif map[y+1][x]=='.':
					point = (x,y+1,-1)
					portal = map[y-1][x] + ch
		if portal!='':
			#print(portal,point)
			if portal not in portals:
				portals[portal]=[]
			portals[portal].append(point)

x,y,_ = portals['AA'][0]
portals['AA']=[(x,y,0)]

x,y,_ = portals['ZZ'][0]
portals['ZZ']=[(x,y,0)]

toTest=[portals['AA'][0]]
tested = {portals['AA'][0]:0}

while len(toTest)>0 and portals['ZZ'][0] not in tested:
	x,y,level = toTest.pop(0)
	for o in offsets:
		xt=x+o[0]
		yt=y+o[1]
		#print(xt,yt,map[yt][xt])
		if map[yt][xt] =='.' and (xt,yt,level) not in tested:
			toTest.append((xt,yt,level))
			tested[(xt,yt,level)]=tested[(x,y,level)]+1
	for p in portals:
		if (x,y) in [(t[0],t[1]) for t in portals[p]]:
			for o in portals[p]:
				xt,yt,cLevel=o
				if (xt,yt)!=(x,y) and (xt,yt,level+cLevel) not in tested and level+cLevel>=0:
					tested[(xt,yt,cLevel+level)]=tested[(x,y,level)]+1
					toTest.append((xt,yt,cLevel+level))

	#print(toTest)

print(tested[portals['ZZ'][0]])
