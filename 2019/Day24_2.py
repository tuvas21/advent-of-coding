fname = 'Day24.inp'

tmap=[]

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip()
		tmap.append([c for c in dat])
		
tmap[2][2]='?'
map = [[['.',]*len(tmap[0]) for y in tmap] for z in range(221)]

for x in range(len(tmap[0])):
	for y in range(len(tmap)):
		map[110][y][x]=tmap[y][x]

offsets = [(0,1,0),(0,-1,0),(1,0,0),(-1,0,0)]

num_neighbors = [[[0 for x in range(len(map[0][0]))] for y in range(len(map[0]))] for z in range(len(map))]

for t in range(200):
	count = 0
	for x in range(len(map[0][0])):
		for y in range(len(map[0])):
			#Offsets are exact x,y, offset in z
			offsets = [(x,y+1,0),(x,y-1,0),(x+1,y,0),(x-1,y,0)]
			if x==3 and y==2:
				offsets += [(4,0,-1),(4,1,-1),(4,2,-1),(4,3,-1),(4,4,-1)]
			elif y==3 and x==2:
				offsets += [(0,4,-1),(1,4,-1),(2,4,-1),(3,4,-1),(4,4,-1)]
			elif x==1 and y==2:
				offsets += [(0,0,-1),(0,1,-1),(0,2,-1),(0,3,-1),(0,4,-1)]
			elif y==1 and x==2:
				offsets += [(0,0,-1),(1,0,-1),(2,0,-1),(3,0,-1),(4,0,-1)]
			if x==4:
				if y==0:
					offsets = [(3,0,0),(4,1,0),(2,1,1),(3,2,1)]
				elif y==4:
					offsets = [(3,4,0),(4,3,0),(2,3,1),(3,2,1)]
				else:
					offsets = [(x-1,y,0),(x,y-1,0),(x,y+1,0),(3,2,1)]
			elif x==0:
				if y==0:
					offsets = [(1,0,0),(0,1,0),(2,1,1),(1,2,1)]
				elif y==4:
					offsets = [(1,4,0),(0,3,0),(2,3,1),(1,2,1)]
				else:
					offsets = [(x+1,y,0),(x,y-1,0),(x,y+1,0),(1,2,1)]
			elif y==0:
				offsets = [(x-1,y,0),(x+1,y,0),(x,y+1,0),(2,1,1)]
			elif y==4:
				offsets = [(x-1,y,0),(x+1,y,0),(x,y-1,0),(2,3,1)]

			for z in range(0,len(map)-1):
				neighbors = 0
				for o in offsets:
					xt = o[0]
					yt = o[1]
					zt = z + o[2]
					if xt>=0 and xt<len(map[0][0]) and yt>=0 and yt<len(map[0]):
						if map[zt][yt][xt]=='#':
							#print((x,y,z),(xt,yt,zt))
							neighbors+=1
					else:
						print(x,y,o)
						quit()
							
				num_neighbors[z][y][x]=neighbors
	for x in range(len(map[0][0])):
		for y in range(len(map[0])):
			for z in range(len(map)):
				neighbors = num_neighbors[z][y][x]
				if x==2 and y==2:
					map[z][y][x]='?'
				elif map[z][y][x]=='#':
					count +=1
					if neighbors != 1:
						map[z][y][x]='.'
				elif neighbors in [1,2]:
					map[z][y][x]='#'
	print(t,count)
	
	# for z in range(198,203):
		# print(200-z)
		# for row in map[z]:
			# print(''.join(row))
		# for row in num_neighbors[z]:
			# print(''.join([str(c) for c in row]))
	# print()

count = 0
for x in range(len(map[0][0])):
	for y in range(len(map[0])):
		for z in range(len(map)):
			if map[z][y][x]=='#':
				count +=1
print(count)