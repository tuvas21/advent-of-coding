from math import atan2

def sort_by_angle(dat):
	if dat[2]>=0:
		return dat[2]
	else:
		return dat[2]+6.2831

fname='Day10.inp'

sum = 0
field = []
locations = []
y=0
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip()
		field.append([c!='.' for c in dat])
		for x in range(len(field[-1])):
			if field[-1][x]:
				locations.append([x,y])
		y+=1

max_seen=0

laser_loc=[0,0]

for x,y in locations:
	num_seen=0
	for xt, yt in locations:
		if x==xt and y==yt:
			continue
		xs=xt-x
		ys=yt-y
		test_limit = max(abs(xs),abs(ys))
		clear = True
		for i in range(2,test_limit+1):
			if xs%i==0 and ys%i==0:
				for f in range(1,i):
					if [x+f*xs//i,y+f*ys//i] in locations:
						clear=False
						#print(x,y,xt,yt,i,f,"Blocked")
						break
				#print (x,y,xt,yt,i)
		if clear:
			num_seen+=1
	
	if num_seen>max_seen:
		print(x,y,num_seen)
		max_seen=num_seen
		laser_loc=[x,y]

x,y=laser_loc

angles=[]
for xt, yt in locations:
	if x==xt and y==yt:
		continue
	xs=xt-x
	ys=yt-y
	test_limit = max(abs(xs),abs(ys))
	clear = True
	for i in range(2,test_limit+1):
		if xs%i==0 and ys%i==0:
			for f in range(1,i):
				if [x+f*xs//i,y+f*ys//i] in locations:
					clear=False
					#print(x,y,xt,yt,i,f,"Blocked")
					break
			#print (x,y,xt,yt,i)
	if clear:
		angles.append([xt,yt,atan2(x-xt,yt-y)-3.14159])

angles=sorted(angles,key = sort_by_angle)
	
print(angles[199])

				
		