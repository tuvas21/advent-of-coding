fname='Day6.inp'

sum = 0
orbits={}
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip().split(')')
		orbits[dat[1]]=dat[0]

YOUKeys=[]
SANKeys=[]

for key in orbits:
	tkey=key
	while tkey in orbits:
		tkey = orbits[tkey]
		sum+=1
		if key == "YOU":
			YOUKeys.append(tkey)
		if key == "SAN":
			SANKeys.append(tkey)
print(sum)

for i in range(1,min(len(YOUKeys),len(SANKeys))):
	if YOUKeys[-i]!=SANKeys[-i]:
		print(len(YOUKeys) + len(SANKeys) - 2*i+2)
		break
		
