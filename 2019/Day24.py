fname = 'Day24.inp'

map=[]

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip()
		map.append([c for c in dat])
map_list = []

offsets = [(0,1),(0,-1),(1,0),(-1,0)]
biodiversity = -1

for row in map:
	print(''.join(c for c in row))
print()

num_neighbors = [[0 for x in range(len(map[0]))] for y in range(len(map))]

while biodiversity not in map_list:
	if biodiversity>=0:
		map_list.append(biodiversity)
	biodiversity = 0
	index = 0
	for x in range(len(map[0])):
		for y in range(len(map)):
			neighbors = 0
			for o in offsets:
				xt=x+o[0]
				yt=y+o[1]
				if xt>=0 and xt<len(map[0]) and yt>=0 and yt<len(map):
					if map[yt][xt]=='#':
						neighbors+=1
			num_neighbors[y][x]=neighbors
	for y in range(len(map)):
		for x in range(len(map[0])):
			neighbors = num_neighbors[y][x]
			if map[y][x]=='#':
				biodiversity+= pow(2,index)
				if neighbors != 1:
					map[y][x]='.'
			elif neighbors in [1,2]:
				map[y][x]='#'
			index += 1
	
	print(biodiversity)
	for row in map:
		print(''.join(c for c in row))
	print()
print(biodiversity)