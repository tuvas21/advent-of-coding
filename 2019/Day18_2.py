fname='Day18_2.inp'
#I changed the input to meet the new requirement as put forth in the problem.

lower='abcdefghijklmnopqrstuvwxyz'
upper='ABCDEFGHIJKLMNOPQRSTUVWXYZ'

class PathBetweenKeys:
	def __init__(self,keysRequired,distance):
		self.keysRequired=keysRequired
		self.distance = distance
		

offsets=[(-1,0),(1,0),(0,1),(0,-1)]

map =[]
mapKeys=[]
mapDoors =[]
ty=0
objects = {}
robot = []
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip()
		map.append([c for c in dat])
		for tx in range(len(dat)):
			if dat[tx]=='@':
				robot.append((tx,ty))
			if dat[tx] in lower:
				objects[dat[tx]]=(tx,ty)
				mapKeys.append(dat[tx])
			elif dat[tx] in upper:
				mapDoors.append(dat[tx])
		ty+=1
		
print(mapKeys)


#This will remove dead end branches. Small improvement, but...
pruned = 0
found = True
while found:
	found=False
	for cx in range(1,len(map[0])-1):
		for cy in range(1,len(map)-1):
			ch = map[cy][cx]
			if ch =='.' or ch in upper:
				numWalls = 0
				neighbors = ''
				for o in offsets:
					tx=cx+o[0]
					ty=cy+o[1]
					ch = map[ty][tx]
					neighbors+=ch
					if ch =='#':
						numWalls +=1
				#print(cx,cy,neighbors,numWalls)
				if numWalls ==3:
					map[cy][cx]='#'
					pruned+=1
					found=True
for row in map:
	print(''.join(row))
print("Pruned ",pruned)

pathDistance={}
requiredKeys={}

quadrant={}

quads=[[] for i in range(4)]

print(len(map),len(map[0]))
	
for obj in objects:
	tested=[]
	emptySpace=[objects[obj]]
	testedLocations = {objects[obj]:PathBetweenKeys('',0)}
	if objects[obj][0]<len(map[0])//2 and objects[obj][1]<len(map)//2:
		quadrant[obj]=0
	elif objects[obj][0]>len(map[0])//2 and objects[obj][1]<len(map)//2:
		quadrant[obj]=1
	elif objects[obj][0]<len(map[0])//2 and objects[obj][1]>len(map)//2:
		quadrant[obj]=2
	elif objects[obj][0]>len(map[0])//2 and objects[obj][1]>len(map)//2:
		quadrant[obj]=3
	else:
		print("What did you do?????",objects[obj],obj)
	#References both ways for ease of use
	quads[quadrant[obj]].append(obj)
	while len(emptySpace)>0:
		cx,cy=emptySpace.pop(0)
		data = testedLocations[(cx,cy)]
		for o in offsets:
			tx=cx+o[0]
			ty=cy+o[1]
			ch = map[ty][tx]
			if (tx,ty) in testedLocations:
				continue
			if ch != '#':
				emptySpace.append((tx,ty))
			if ch =='.' or ch in lower or ch =='@':
				testedLocations[tx,ty]=PathBetweenKeys(data.keysRequired,data.distance+1)
			elif ch in upper:
				testedLocations[tx,ty]=PathBetweenKeys(data.keysRequired+ch,data.distance+1)
				
			if ch in lower:
				pathDistance[(obj,ch)]=data.distance+1
				requiredKeys[(obj,ch)]=data.keysRequired
print(robot)

for i in range(4):
	print(quads[i])

for rl in robot:
	tested=[]
	emptySpace=[rl]
	testedLocations = {rl:PathBetweenKeys('',0)}
	while len(emptySpace)>0:
		cx,cy=emptySpace.pop(0)
		data = testedLocations[(cx,cy)]
		for o in offsets:
			tx=cx+o[0]
			ty=cy+o[1]
			ch = map[ty][tx]
			if (tx,ty) in testedLocations:
				continue
			if ch != '#':
				emptySpace.append((tx,ty))
			if ch =='.' or ch in lower or ch =='@':
				testedLocations[tx,ty]=PathBetweenKeys(data.keysRequired,data.distance+1)
			elif ch in upper:
				testedLocations[tx,ty]=PathBetweenKeys(data.keysRequired+ch,data.distance+1)
				
			if ch in lower:
				pathDistance[('@',ch)]=data.distance+1
				requiredKeys[('@',ch)]=data.keysRequired

print("Total connections found:",len(pathDistance))

#This is based on the range I'm getting from testing of some possible solutions
minDistance = 5000

def function_loop(keys,rLoc,distance):
	global minDistance
	combos = []
	if distance > minDistance:
		return []
	
	if len(keys)==len(mapKeys) and minDistance > distance:
		print(distance)
		minDistance = distance
		return[]
		
	
	for qi in range(4):
		cKey = rLoc[qi]
		canTravelAllQuadrant = True
		quadrantCombos = []
		for obj in quads[qi]:
			if obj not in keys:
				dist = distance+pathDistance[(cKey,obj)]
				if dist > minDistance:
					continue
				hasAllKeys=True
				for k in requiredKeys[(cKey,obj)]:
					if k.lower() not in keys:
						hasAllKeys=False
						canTravelAllQuadrant = False
						break
				if hasAllKeys:
					rl = rLoc[:]
					rl[qi]=obj
					quadrantCombos.append((dist,rl,keys+obj))
		if canTravelAllQuadrant and len(quadrantCombos)>0:
			#Only try these combinations, we know we need to visit them all, so...
			return quadrantCombos
		else:
			combos+=quadrantCombos
			
	return combos
	
print(len(mapKeys))
#Kick start the process	
nCombos = []
for obj in objects:
	rl = ['@','@','@','@']
	rl[quadrant[obj]]=obj
	if requiredKeys[('@',obj)]=='':
		dist = pathDistance[('@',obj)]
		nCombos.append((dist,rl,obj))
		
while len(nCombos)>0:
	level=len(nCombos[0][2])
	print("New Level ",len(nCombos),level)
	#This will filter the numbers to speed things up
	mxv = max(nCombos)[0]
	mnv=min(nCombos)[0]
	print(mnv,mxv)
	# Tries to keep between 1-3 million combinations tops to test. Don't filter if the range is very small
	if len(nCombos)>500000 and mxv-mnv>300:
		if len(nCombos)>20000000:
			limit = 0.2
		elif len(nCombos)>10000000:
			limit = 0.25
		elif len(nCombos)>5000000:
			limit = 0.3
		elif len(nCombos)>2000000:
			limit = 0.35
		elif len(nCombos)>1000000:
			limit = 0.4
		else:
			limit = 0.45
		combos = [c for c in nCombos if c[0]<(mxv-mnv)*limit+mnv]
		print ("Filtered: New count is ",len(combos),limit)
	else:
		combos = nCombos[:]
	if len(combos)==0:
		break
	nCombos = []
	for c in combos:
		ret = function_loop(c[2],c[1],c[0])
		nCombos += ret
	
print(minDistance)
	
			