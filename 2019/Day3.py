fname='Day3.tst'

cx=10000
cy=10000
g=[[0 for x in range(20000)] for y in range(20000)]

ret=1000000000
wire=1
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		x=cx
		y=cy
		for cmd in content.split(','):
			if cmd[0]=='U':
				cmdx=0
				cmdy=1
			elif cmd[0]=='D':
				cmdx=0
				cmdy=-1
			elif cmd[0]=='R':
				cmdx=1
				cmdy=0
			elif cmd[0]=='L':
				cmdx=-1
				cmdy=0
			iters=int(cmd[1:])
			for i in range(iters):
				x+=cmdx
				y+=cmdy
				if g[x][y]==1 and wire==2:
					print(x,y)
					ret=min(ret,abs(cx-x)+abs(cy-y))
					g[x][y]=-1
				else:
					g[x][y]=wire
		wire+=1
					
print(ret)
		
