fname='Day12.inp'

def mag(vector):
	return sum([abs(n) for n in vector])


location = []
velocity = []
y=0
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip().split(',')
		
		x=int(dat[0].split('=')[1])
		y=int(dat[1].split('=')[1])
		z=int(dat[2].split('=')[1][:-1])
		#print(x,y,z)
		
		location.append([x,y,z])
		velocity.append([0,0,0])
		

for i in range(1000):
	#print(i+1)
	for p in range(len(location)):
		for c in range(len(location)):
			for a in range(3):
				if location[p][a]>location[c][a]:
					velocity[p][a]-=1
				elif location[p][a]<location[c][a]:
					velocity[p][a]+=1
	for p in range(len(location)):
		for a in range(3):
			location[p][a]+=velocity[p][a]
		#print(location[p])
		#print(velocity[p])
		#print(mag(location[p])*mag(velocity[p]))
total = 0
for p in range(len(location)):
	total +=mag(location[p])*mag(velocity[p])
print(total)