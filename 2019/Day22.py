fname='Day22.inp'

def deal_new_stack(cards):
	return [c for c in reversed(cards)]
	
def cut(cards,n):
	return cards[n:]+cards[:n]

def deal_increment(cards,n):
	i=0
	ret = [0,]*len(cards)
	for c in range(len(cards)):
		ret[i]=cards[c]
		i = (i + n)%len(cards)
	return ret

cards = [a for a in range(10007)]

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip()
		if dat.startswith('deal with increment'):
			cards = deal_increment(cards,int(dat[20:]))
		elif dat.startswith('cut'):
			cards = cut(cards,int(dat[4:]))
		elif dat=='deal into new stack':
			cards = deal_new_stack(cards)
			
print (cards.index(2019))



