from math import ceil

fname='Day14.inp'

def getVals(raw):
	parsed = raw.strip().split()
	return (parsed[1],int(parsed[0]))
	
def test_current(curr_state):
	for k in curr_state:
		if k!= 'ORE' and curr_state[k]>0:
			return False
	return True

data = {}

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat = content.strip().split("=>")
		outv=getVals(dat[1])
		input_vals = []
		for inp in dat[0].split(','):
			input_vals.append (getVals(inp))
		data[outv]=input_vals
		
curr = {'FUEL':1}
while test_current(curr) == False:
	changed=False
	for c in curr:
		for k in data:
			if k[0]==c and  curr[c]>0:
				factor = ceil(curr[c]/ k[1])
				curr[c]-=factor*k[1]
				for inp in data[k]:
					if inp[0] not in curr:
						curr[inp[0]]=0
					curr[inp[0]]+=inp[1]*factor
				changed=True
				break
		if changed:
			break
			
print (curr['ORE'])

ore_required = curr['ORE']

curr = {'FUEL':1,'ORE':-1000000000000}
fuel_produced = 0
while curr['ORE']<0:
	changed=False
	if test_current(curr):
		new_val = curr['ORE']//-ore_required
		if new_val==0:
			new_val=1
		curr['FUEL']+=new_val
		fuel_produced+=new_val
		print(fuel_produced,curr['ORE'])
	for c in curr:
		for k in data:
			if k[0]==c and  curr[c]>0:
				factor = ceil(curr[c]/ k[1])
				curr[c]-=factor*k[1]
				for inp in data[k]:
					if inp[0] not in curr:
						curr[inp[0]]=0
					curr[inp[0]]+=inp[1]*factor
				changed=True
				break
		if changed:
			#print(curr)
			break

print (fuel_produced)
				
				