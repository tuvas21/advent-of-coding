import struct
import array
import sys

#fid=open("debug.log","w");
fidinput=open("input.log","rb");
fidcommands=open("command.log","wb");
inp="challenge.bin"
#inp="rr.bin"

class _Getch:
    """Gets a single character from standard input.  Does not echo to the screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()

class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


getch = _Getch()


debugMem=False
debug=False
debugProgram=False

register=[0]*8
prog=array.array("H",[0]*32768)
f=open(inp,"rb")
ind=0
while(True):
	tmp=f.read(2)
	if (not tmp):
		break
	prog[ind]=struct.unpack("<H",tmp)[0]
	ind+=1
f.close()

ExecutionPointer=0	# Pointer in # of instructions
stack=[]

def peek(mem=-1,registerValue=True):
	global ExecutionPointer
	if (mem==-1):
		mem=ExecutionPointer;
		ExecutionPointer+=1;
	val=prog[mem]
#	print(val)
	if (registerValue and val>32767):
		regIndex=val-32768
		val=register[regIndex]
		if debugMem or debug:
			if regIndex==7:
				print(register)
#				fid.write("registers="+str(register));
#				fid.write("Register %i %i %i\n"%(regIndex,val,mem))
#				for i in range(-5,10):
#					fid.write("exp: %2i %i\n"%(i,peek(ExecutionPointer-i,False)))
#				for i in range(-5,10):
#					fid.write("mem: %2i %i\n"%(i,peek(mem-i,False)))
#				fid.write("stack="+str(stack))

	return val

#874 881 12521 24196 24956	
i=0
#while(True):
#	a=peek(i,False)
#	if a==30000:
#		print (i)
#		break
#	i+=1
#quit()
	
def readMem(loc):
	if (loc>32767):
		if debugMem or debug:
			if loc==32775:
				print(register)
				print("Register %i %i %i"%(loc,register[loc-32768],peek(register[loc-32768])))
		return peek(register[loc-32768])
	else:
		#print(peek(loc))
		return peek(loc)	
	
def writeMem(loc,val,op):
	if (loc>32767):
		if debug and loc==32775:
			print((loc,register[loc-32768],val,op))
		register[loc-32768]=val
	else:
		if debugMem:
			print("W %i %i"%(loc,val))
		prog[loc]=val

def printToDebug(log):
	pass
#	fid.write(str(log))
#	fid.write("\n")
#	fid.flush();

#25975 25974 26006 0 101 0 0 0
#6080 16 6124 1 2826 32 3 10 101 0
#1800
def debugMode():
	global ExecutionPointer,stack,debug,debugProgram
	while (True):
		print(outString)
		print(register)
		print(stack)
		print(ExecutionPointer)
		code=input("Debug Mode Option>")
		if code=="c":
			return True
		elif code=="q":
			raise Exception("Done with looping>")
		elif code=="d":	#Toggles debug statements
			debug= not debug
		elif code[0]=="r":	#Sets register
			val=int(input("What value?>"))
			register[int(code[1])]=val
		elif code=="e":		# Sets execution pointer, should be last thing done
			val=int(input("Where should the execution pointer be set to?"))
			ExecutionPointer=val
			return False
		elif code=="s":		#Puts a value on the stack
			val=input("What should the stack be?>")
			stack=list(map(int,val.split(" ")))
		elif code=="program":
			debugProgram=not debugProgram
		elif code=="m":
			tempstr=""
			try:
				for i in range(len(prog)):
					op=peek(i)
					if op==19:
						ch=peek(i+1)
						if ch<255:
							tempstr+=chr(ch)
			finally:
				print(tempstr)
	
fidText=open("outputText.txt","w")
def logText(outString):
	if (outString!=""):
		fidText.write(outString)
		print(outString)
		outString=""
	return outString
			
debugProgram=True
		
outString=""
try:
	if len(stack)>20:
		debugMode()
	op=1
	while op!=0:
		if debugProgram:
			if ExecutionPointer==5491:
				print(register)
#				writeMem(32769,7,-1)	#More cheating
				writeMem(32768,6,-1)	#More cheating
				writeMem(32775,25734,-1)	#More cheating 27194 Tried 4681 6422 16585 18502 22138 22328 25970 27203 29127 29789
			elif ExecutionPointer==6027:
				writeMem(32768,0,-1)
			elif ExecutionPointer==5449:
				writeMem(32775,1,-1)
			elif ExecutionPointer==5486:
				writeMem(32768,2,-1)		# This will allow for simple testing.
		op=peek()
		if debugProgram:
			printToDebug("%i\t%i\t%i\t%i\t%i\t%i"%(ExecutionPointer,op,peek(ExecutionPointer,False),peek(ExecutionPointer+1,False),peek(ExecutionPointer+2,False),len(stack)))
			if peek(ExecutionPointer,False)==32775 or peek(ExecutionPointer+1,False)==32775 or peek(ExecutionPointer+2,False)==32775:
				printToDebug(stack)
				printToDebug(register)
		if op==0:
			break;
		elif op==1:
			a=peek(registerValue=False)
			b=peek()
			if debug:
				printToDebug("%i %i %i"%(op,a,b))
			writeMem(a,b,op)
#			if debugProgram and peek(ExecutionPointer-1,False)==32775:
#				writeMem(32775,0,-1)		# Attempts to cheat
		elif op==2:
			a=peek()
			stack.append(a);
		elif op==3:
			a=peek(registerValue=False)
			writeMem(a,stack.pop(),op);
		elif op==4:
			a=peek(registerValue=False)
			b=peek()
			c=peek()
			if debug:
				printToDebug((op,a,b,c))
			if (b==c):
				writeMem(a,1,op)
			else:
				writeMem(a,0,op)
		elif op==5:
			a=peek(registerValue=False)
			b=peek()
			c=peek()
			if debug:
				printToDebug((op,a,b,c))
			if (b>c):
				writeMem(a,1,op)
			else:
				writeMem(a,0,op)
		elif op==6:
			ExecutionPointer=peek();
		elif op==7:
			a=peek()
			b=peek()
			if debug:
				printToDebug((op,a,b))
			if(a!=0):
				ExecutionPointer=b
		elif op==8:
			a=peek()
			b=peek()
			if debug:
				printToDebug((op,a,b))
#			print(register)
#			print("%i %i %i"%(op,a,b))
			if(a==0):
				ExecutionPointer=b
		elif op==9:
			a=peek(registerValue=False)
			b=peek()
			c=peek()
			if debug:
				printToDebug((op,a,b,c))
			writeMem(a,(b+c)%32768,op)
		elif op==10:
			a=peek(registerValue=False)
			b=peek()
			c=peek()
			writeMem(a,(b*c)%32768,op)
		elif op==11:
			a=peek(registerValue=False)
			b=peek()
			c=peek()
			writeMem(a,(b%c)%32768,op)
		elif op==12:
			a=peek(registerValue=False)
			b=peek()
			c=peek()
			writeMem(a,b&c,op)
		elif op==13:
			a=peek(registerValue=False)
			b=peek()
			c=peek()
			writeMem(a,b|c,op)
		elif op==14:
			a=peek(registerValue=False)
			b=peek()
			writeMem(a,(~b)%32768,op)
		elif op==15:
#			print(register)
			a=peek(registerValue=False)
			b=peek(registerValue=False)
			if debugMem:
				print("Reading %i %i %i"%(a,b,readMem(b)))
			writeMem(a,readMem(b),op)
#			print(register)
		elif op==16:
			a=peek()
			b=peek()
			writeMem(a,b,op)
			if debugMem:
				print((op,a,b,peek(b)))
				print(register)
		elif op==17:
			a=peek()
			stack.append(ExecutionPointer)
			#print((op,a))
			#print(stack)
			ExecutionPointer=a
		elif op==18:
			ExecutionPointer=stack.pop()
		elif (op==19):
			a=peek()
			outString+=chr(a)
			if debugProgram:
				printToDebug("%i %i %s"%(op,a,chr(a)))
		elif (op==20):
#			debugMem=True
			a=peek(registerValue=False)
			printCursor=outString!=""
			outString=logText(outString)
			if printCursor:
				print(">",end="")
				sys.stdout.flush()	
			ch=3
			breakLoop=False
			while (not breakLoop):
#				print(ch)
				if fidinput.closed:
					rawch=getch()
					ch=struct.unpack("B",rawch)[0]
					fidcommands.write(rawch)
				else:
#					print("From File")
					tmp=fidinput.read(1)
#					print(tmp)
					if not tmp:
						fidinput.close()
						continue
					ch=struct.unpack("B",tmp)[0];
					if ch==13:
						continue
				if (ch==13):
					ch=10
					break
				elif ch==3:
					breakLoop=debugMode()
					if breakLoop:
						break
				else:
					break
	#				raise Exception("Raised Error!")
			if breakLoop:
				continue
			print(chr(ch),end="")
			sys.stdout.flush()
			#theInput=input()
#			for ch in theInput:
#			print("Writing %i %i"%(a,ch))
			writeMem(a,ch,op)
			a+=1
#			writeMem(a,10)
		elif (op==21):
			continue
		else:	
			print("Undefined operation %i"%op);
finally:
#	f.close()
#	fid.close()
	logText(outString)
	print(register)
	print(stack)
	print(ExecutionPointer)
	fidcommands.close()
	fidinput.close()
	fidText.close()
