import random

grid=[	[1,"-",8,"*"],
		["*",11,"*",4],
		[18,"-",4,"+"],
		["*",9,"-",22]]
		
def scorePath(path):
	x=path[0][0]
	y=path[0][1]
	score=grid[x][y]
	for i in range(1,len(path),2):
		op=grid[path[i][0]][path[i][1]]
		val=grid[path[i+1][0]][path[i+1][1]]
#		print((op,val,path[i+1]))
		if op=="*":
			score*=val
		elif op=="-":
			score-=val
		elif op=="+":
			score+=val
		else:
			print("Invalid op %s for %i %i"%(str(op),x,y))
	return score

def findRandomPath():
	x=3
	y=3
	path=[(x,y)]
	while (x!=0 or y!=0):
		if x==3 and y==3:
			path=[(x,y)]
		possibleLoc=[]
		if x<3:
			possibleLoc.append((x+1,y))
		if x>0:
			possibleLoc.append((x-1,y))
		if y<3:
			possibleLoc.append((x,y+1))
		if y>0:
			possibleLoc.append((x,y-1))
		
		newLoc=random.choice(possibleLoc)
		path.append(newLoc)
		x,y=newLoc
#		print((x,y))
	return path
	
path=findRandomPath()
print(scorePath(path))
print(path)


minPath=20
while(True):
	path=findRandomPath()
	if len(path)>=minPath:
		continue
	if scorePath(path)==30:
		minPath=len(path)
		print(path)
#	break;
		
#while True: