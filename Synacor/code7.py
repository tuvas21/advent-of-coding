import time


r0 = 4
r1 = 1
r7 = 1
#Set to remind me of typical inputs

#Used to make the printing easier
def findStackChange(stack):
	if stack[-1]>1:
		return len(stack),stack[-1]-1
		
	for i in range(len(stack)-1,-1,-1):
		if stack[i]!=1:
			return i,stack[i]-1
	return -1,0

fid=open("temp.txt","w")		#Used to log some things to a file
# This bit of code I used to run the script in pure python, which was somewhat faster than testing ideas in the synacor machine langauge.
# I logged the stack to a file to make things easier to optimize. I found the number of 1s was the key item in the pattern, and thus only logged those.
def code_6049():
	global r0,r1,r7,lookahead_counter
	if r0 == 0:
		r0 = r1 + 1
		return

	# When r1 reaches zero, decrement r0, and return r1 to r7(test value)
	if r1 == 0:
		r0 -= 1
		r1 = r7
#		r7 = 0	#debug code		I used this to do some quick verification using the ingame code, with a similar modification
		index,value=findStackChange(stack)
		fid.write(str(stack[:index+1])+" %i %i\n"%(len(stack),len(stack)-index-1))	#Logs the 
		code_6049()
		return

	stack.append(r0)
	r1 -= 1
	code_6049()
	r1 = r0
	r0 = stack.pop()
	r0 -= 1
	code_6049()
	


	
# This function started as a look ahead counter to assist with a function I was working on. I decided after a while I just needed this function, but never changed the name.
# The basic idea is to keep track of the number of 1s when R1 is reset to R7.
# I determined there are a number of patterns, and with some careful debugging, was able to find these patterns.
# I could explain more if you'd like.
helper={}
def lookAhead(num,numOnes,r7,clearHelper=True):
	global helper	#Stores numbers off to not have to compute them every time.
	if clearHelper:
		helper={};
	if (num,numOnes,r7) in helper:
		return helper[(num,numOnes,r7)]
#	fid.write("%i %i %i\n"%(num,numOnes,r7))
	if num==1:
		return (numOnes+1) %32768
	elif numOnes==0:
		if num==2:
			return (r7*2+1)%32768
		elif num==3:
			return (r7*r7*r7+4*r7*r7+5*r7+1)%32768
		tnumOnes=0;
		for i in range(r7):
			tnumOnes=lookAhead(num-1,tnumOnes,r7,False)
			if tnumOnes==0:
				break;
#			print((num-1,tnumOnes))
		if tnumOnes==0:
			if num==4:
				print((num,numOnes,r7))
			#This prevents an infinite loop. From the code, this should be what happens in reality, although I did not extensively study it.
			return 0
		tnumOnes=lookAhead(num,tnumOnes,r7,False)%32768
		helper[(num,numOnes,r7)]=tnumOnes % 32768
		return tnumOnes % 32768
	elif num==2:
		temp=numOnes+r7+1 % 32768
		return temp
	elif num==3:
		return ((numOnes+r7+2)%32768*(r7+1)-1)
	elif num==4:
		ret=1
		for i in range((2+r7+numOnes)%32768):
			ret*=r7+1
		ret=(ret-1)*((r7+1)*(r7+1))
		return ret//r7-1 % 32768
	else:
		numNewVal=(numOnes+r7+1)	#One of the trickiest things in this challenge was to not mod this by 32768

		if num==2:
			tnumOnes=numNewVal
		else:
			tnumOnes=0
#		print(numNewVal)
			for i in range(numNewVal):
				tnumOnes=lookAhead(num-1,tnumOnes,r7,False) % 32768
		helper[(num,numOnes,r7)]=tnumOnes%32768;
		return tnumOnes%32768

#The rest of this is either for doing verification tests, or computing the value
		
lastChange=[0]*3
lastOnes=[0]*3
	
stack=[4]
r7=2
#simple_algo()

stack=[]
r7=2
r1=1
r0=4
#code_6049()
#print(r0)
#print(lookAhead(3,0,10)+9)

#print(lookAhead(2,0,2))


#This loop I used for testing various numbers, to get a table and do some linar algebra with.
for r7 in range(1,40):
	for i in range(1):
#		print("%i\t%i\t%i"%(r7,i,lookAhead(3,i,r7)))
		pass
		
lastChange=[0]*3
lastOnes=[0]*3

#These tests were to assist me in verifying the routine was correct.
#I will admit that I eventually sought a bit of help for some test cases, which is where the numbers came from.


#print(lookAhead(2,0,2)) #8
#print(lookAhead(2,0,3)) #11
#print(lookAhead(3,0,2)) #35
#print(lookAhead(4,0,0))
print((lookAhead(4,0,1)+2)%32768==32765)
print((lookAhead(4,0,2)+3)%32768==13234)
print((lookAhead(4,0,3)+4)%32768==21843)
print((lookAhead(4,0,4)+5)%32768==14930)
print((lookAhead(4,0,5)+6)%32768==13105)
print((lookAhead(4,0,6)+7)%32768==29574)
print((lookAhead(4,0,7)+8)%32768==4679)
print((lookAhead(4,0,8)+9)%32768==17970)
print((lookAhead(4,0,9)+10)%32768==29125)
print((lookAhead(4,0,18502)+18503)%32768==20422)


stime=time.time()
#At this point, everything looks good, trying to actually find the value.
for r7 in range(1,32768):
	if r7%100==0:
		print("Value: %i time elapsed: %f s"%(r7,(time.time()-stime)))
		stime=time.time()
	val=0;
	val=(lookAhead(4,0,r7)+r7+1) % 32768
	#This is really a holdover from prior times, but I was afraid of an off by one or two error.
	#As a result, I printed anything close the the value, figuring I could test more than one if required.
	if (val<=10 and val>2):
		print("%i %i"%(r7,val))
#	if val==6:
#		quit()
#	finally:
#		print("%i %i"%(r7,r1))
