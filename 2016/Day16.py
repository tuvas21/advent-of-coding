a = '10001110011110000'
sz=35651584

while len(a)<sz:
	a = a + '0' + ''.join(['0' if c=='1' else '1' for c in a[::-1]])
data = a[:sz]
while len(data) % 2 == 0:
	next = ''
	for i in range(0,len(data),2):
		next+= '1' if data[i]== data[i+1] else '0'
	data = next
print(data)