fname = 'Day20.inp'

bots = [[] for i in range(1250)]
comparisons = [[] for i in range(250)]

lowest_value= 0
max_value = 4294967295

#max_value = 9

pairs = []


with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		dat = [int(c) for c in content.split('-')]
		pairs.append(dat)
		
#pairs = [(0,4),(3,5),(8,9)]

pairs.sort()
first_print = True

num_valid = 0

for p in pairs:
	if lowest_value < p[0]:
		if first_print:
			print(lowest_value)
			first_print = False
		num_valid+= p[0] - lowest_value
	lowest_value = max(lowest_value,p[1] + 1)
num_valid += max(0,max_value - lowest_value + 1)
print(num_valid)