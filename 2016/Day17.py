import hashlib

inp = 'vkjiggvb'

testList = [(0,0,'')]
step = 0
validChars = 'bcdef'

offset = [(0,-1),(0,1),(-1,0),(1,0)]
dirs = 'UDLR'

printFirst = True
maxDist = 0

while len(testList)>0:
	nextList = []
	for x,y,dir in testList:
		if x==3 and y==3:
			if printFirst:
				print(step,dir)
			maxDist = step
			printFirst = False
			continue
		text = inp + dir
		m = hashlib.md5()
		m.update(text.encode('ASCII'))
		oh = m.hexdigest()
		for i in range(len(dirs)):
			if oh[i] in validChars:
				o = offset[i]
				xt = x + o[0]
				yt = y + o[1]
				if xt >= 0 and yt >=0 and xt < 4 and yt <4:
					nextList.append((xt,yt,dir+dirs[i]))
	testList = nextList
	step +=1
	#print(step,len(testList))
print(maxDist)
