fname = 'Day21.inp'

pw = [x for x in 'abcdefgh']
instructions = []

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		instructions.append(content)
		dat = content.split()
		
		if content.startswith('swap position'):
			a = int(dat[2])
			b = int(dat[5])
			pw[a],pw[b] = pw[b],pw[a]
		elif content.startswith('swap letter'):
			a = pw.index(dat[2])
			b = pw.index(dat[5])
			pw[a],pw[b] = pw[b],pw[a]
		elif content.startswith('rotate based'):
			a = pw.index(dat[6])
			if a >= 4:
				a += 1
			a += 1
			a = a % len(pw)
			pw = pw[-a:] + pw[:-a]
		elif content.startswith('rotate left'):
			a = int(dat[2])
			pw = pw[a:] + pw[:a]
		elif content.startswith('rotate right'):
			a = int(dat[2])
			pw = pw[-a:] + pw[:-a]
		elif content.startswith('reverse positions'):
			a = int(dat[2])
			b = int(dat[4])+1
			pw = pw[:a] + pw[a:b][::-1] + pw[b:]
		elif content.startswith('move'):
			a = int(dat[2])
			b = int(dat[5])
			if b>a:
				pw = pw[:a] +pw[a+1:b+1] + [pw[a]] + pw[b+1:]
			else:
				pw = pw[:b] + [pw[a]] +pw[b:a]  + pw[a+1:]
		else:
			print('You missed a command dummy!')
			print(content)
print(''.join(pw))

pw = [c for c in 'fbgdceah']

for content in instructions[::-1]:
	dat = content.split()
	
	if content.startswith('swap position'):
		a = int(dat[2])
		b = int(dat[5])
		pw[a],pw[b] = pw[b],pw[a]
	elif content.startswith('swap letter'):
		a = pw.index(dat[2])
		b = pw.index(dat[5])
		pw[a],pw[b] = pw[b],pw[a]
	elif content.startswith('rotate based'):
		b = pw.index(dat[6])
		a = [1, 1, 6, 2, 7, 3, 0, 4][b]
		pw = pw[a:] + pw[:a]
	elif content.startswith('rotate right'):
		a = int(dat[2])
		pw = pw[a:] + pw[:a]
	elif content.startswith('rotate left'):
		a = int(dat[2])
		pw = pw[-a:] + pw[:-a]
	elif content.startswith('reverse positions'):
		a = int(dat[2])
		b = int(dat[4])+1
		pw = pw[:a] + pw[a:b][::-1] + pw[b:]
	elif content.startswith('move'):
		b = int(dat[2])
		a = int(dat[5])
		if b>a:
			pw = pw[:a] +pw[a+1:b+1] + [pw[a]] + pw[b+1:]
		else:
			pw = pw[:b] + [pw[a]] +pw[b:a]  + pw[a+1:]
	else:
		print('You missed a command dummy!')
		print(content)
print(''.join(pw))
