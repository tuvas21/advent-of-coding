fname = 'Day25.inp'

instructions=[]

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		dat = content.split()
		instructions.append(dat)

nextA = 0
while True:
	reg=[nextA,0,0,0]
	instr = 0
	print(nextA)
	nextOutput = 0

	while instr < len(instructions) :
		cmd = instructions[instr]
		
		if cmd[0]=='cpy':
			outi = ord(cmd[2])-ord('a')
			try:
				outv= int(cmd[1])
			except:
				outv = reg[ord(cmd[1])-ord('a')]
			reg[outi]=outv
			instr+=1
			continue
		
		regi = ord(cmd[1])-ord('a')
		if cmd[0]=='inc':
			reg[regi]+=1
		elif cmd[0]=='dec':
			reg[regi]-=1
		elif cmd[0]=='out':
			if nextOutput == reg[regi]:
				nextOutput = [1,0][nextOutput]
			else:
				break
		elif cmd[0]=='jnz':
			try:
				testv = int(cmd[1])
			except:
				testv = reg[regi]
			try:
				jumpv = int(cmd[2])
			except:
				jumpv = reg[ord(cmd[2])-ord('a')]
			if testv != 0:
				instr += jumpv
				continue
		elif cmd[0]=='tgl':
			try:
				outv= int(cmd[1])
			except:
				outv = reg[regi]
			try:
				pinst= instructions[outv+instr][0]
				if pinst in ['tgl','dec']:
					instructions[outv+instr][0] = 'inc'
				elif pinst == 'inc':
					instructions[outv+instr][0] = 'dec'
				elif pinst == 'jnz':
					instructions[outv+instr][0] = 'cpy'
				elif pinst == 'cpy':
					instructions[outv+instr][0] = 'jnz'
				else:
					print("Invalid instruction: ", pinst)
			except:
				pass
		else:
			print("You made a very BAAAAAD mistake, sheep.", cmd)
		instr +=1
	nextA += 1