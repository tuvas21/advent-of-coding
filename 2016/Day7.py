fname = 'Day7.inp'

def abba(inp):
	for i in range(len(inp)-3):
		if inp[i]==inp[i+3] and inp[i+1]==inp[i+2] and inp[i]!=inp[i+1]:
			return True
	return False

def test_tls(inp):
	isValid = False	#Has found TLS outside brackets
	index = 0
	for i in range(len(inp)):
		if inp[i]=='[':
			isValid = isValid or abba(inp[index:i])
			index = i+1
		elif inp[i]==']':
			if abba(inp[index:i]):
				return False
			index = i+1
	isValid = isValid or abba(inp[index:])
	return isValid
	
def test_ssl(inp):
	aba =[]
	bab = []
	index = 0
	for i in range(len(inp)):
		if inp[i]=='[':
			aba.append(inp[index:i])
			index = i+1
		elif inp[i]==']':
			bab.append(inp[index:i])
			index = i+1
	aba.append(inp[index:])
	for s in aba:
		for i in range(len(s)-2):
			if s[i]!= s[i+1] and s[i]==s[i+2]:
				test= s[i+1]+s[i]+s[i+1]
				for s2 in bab:
					if test in s2:
						return True
	return False

ret = 0
ret2 = 0

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		if test_tls(content):
			ret +=1
		if test_ssl(content):
			ret2 +=1
		
print(ret)
print(ret2)
