fname = 'Day23_2.inp'

instructions=[]

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		dat = content.split()
		instructions.append(dat)

if True:
	reg=[12,0,0,0]
	instr = 0

	while instr < len(instructions):
		cmd = instructions[instr]
		
		if cmd[0]=='cpy':
			outi = ord(cmd[2])-ord('a')
			try:
				outv= int(cmd[1])
			except:
				outv = reg[ord(cmd[1])-ord('a')]
			reg[outi]=outv
			instr+=1
			continue
		elif cmd[0]=='nll':
			instr+=1
			continue
		
		regi = ord(cmd[1])-ord('a')
		if cmd[0]=='mul':
			a = reg[ord(cmd[2])-ord('a')]
			b = reg[ord(cmd[3])-ord('a')]
			reg[regi]= a * b
		elif cmd[0]=='inc':
			reg[regi]+=1
		elif cmd[0]=='dec':
			reg[regi]-=1
		elif cmd[0]=='jnz':
			try:
				testv = int(cmd[1])
			except:
				testv = reg[regi]
			try:
				jumpv = int(cmd[2])
			except:
				jumpv = reg[ord(cmd[2])-ord('a')]
			if testv != 0:
				instr += jumpv
				continue
		elif cmd[0]=='tgl':
			try:
				outv= int(cmd[1])
			except:
				outv = reg[regi]
			try:
				pinst= instructions[outv+instr][0]
				print(outv)
				if pinst in ['tgl','dec']:
					instructions[outv+instr][0] = 'inc'
				elif pinst == 'inc':
					instructions[outv+instr][0] = 'dec'
				elif pinst == 'jnz':
					instructions[outv+instr][0] = 'cpy'
				elif pinst == 'cpy':
					instructions[outv+instr][0] = 'jnz'
				else:
					print("Invalid instruction: ", pinst)
			except:
				pass
		else:
			print("You made a very BAAAAAD mistake, sheep.", cmd)
		instr +=1
	print(reg)
			
	