fname = 'Day24.inp'

grid=[]

y = 0
startx = 0
starty = 0

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		grid.append(content)
		if content.find('0')>0:
			startx = content.find('0')
			starty = y
		y += 1

print(startx,starty)
print(len(grid))

neighbors = [(0,1),(0,-1),(1,0),(-1,0)]
visitList = [(startx,starty,1)]
hasVisted = set([(startx,starty,1)])
steps = 1
while True:
	nextList = []
	for next in visitList:
		x,y,code = next
		for o in neighbors:
			tx = x + o[0]
			ty = y + o[1]
			c = grid[ty][tx]
			tempCode = code
			if c == '#':
				continue
			elif c != '.':
				k = 2**int(c)
				if k & code == 0:
					tempCode += k
			if tempCode == 255 and tx == startx and ty == starty:
				print(steps)
				quit()
			arg = (tx,ty,tempCode)
			if arg not in hasVisted:
				nextList.append(arg)
				hasVisted.update([arg])
			
	visitList = nextList
	steps += 1
