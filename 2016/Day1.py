inp ='R2, L5, L4, L5, R4, R1, L4, R5, R3, R1, L1, L1, R4, L4, L1, R4, L4, R4, L3, R5, R4, R1, R3, L1, L1, R1, L2, R5, L4, L3, R1, L2, L2, R192, L3, R5, R48, R5, L2, R76, R4, R2, R1, L1, L5, L1, R185, L5, L1, R5, L4, R1, R3, L4, L3, R1, L5, R4, L4, R4, R5, L3, L1, L2, L4, L3, L4, R2, R2, L3, L5, R2, R5, L1, R1, L3, L5, L3, R4, L4, R3, L1, R5, L3, R2, R4, R2, L1, R3, L1, L3, L5, R4, R5, R2, R2, L5, L3, L1, L1, L5, L2, L3, R3, R3, L3, L4, L5, R2, L1, R1, R3, R4, L2, R1, L1, R3, R3, L4, L2, R5, R5, L1, R4, L5, L5, R1, L5, R4, R2, L1, L4, R1, L1, L1, L5, R3, R4, L2, R1, R2, R1, R1, R3, L5, R1, R4'

# N E S W

grid = [[False,]*2000 for i in range(2000)]
dir = 0
x=0
y=0
dat = inp.split(', ')
printSecondPart = True
for i in dat:
	if i[0]=='R':
		dir = dir + 1
	elif i[0]=='L':
		dir = dir -1
	dir = (4+dir) % 4
	
	steps = int(i[1:])
	
	ix =0
	iy =0
	
	if dir == 0:
		iy = 1
	elif dir == 1:
		ix = 1
	elif dir==2:
		iy = -1
	elif dir==3:
		ix = -1
	else:
		print ("SOMETHING BAD HAPPENED!!!!",dir)
		quit()
	for s in range(steps):
		x += ix
		y += iy
		if printSecondPart:
			if grid[1000+x][1000+y]:
				print(x,y,abs(x)+abs(y))
				printSecondPart = False
			grid[1000+x][1000+y] = True
print(x,y,abs(x)+abs(y))
		