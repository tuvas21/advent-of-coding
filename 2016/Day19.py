inp = 3018458

stillIn = [i for i in range(inp)]
firstOut = 1

while len(stillIn)>1:
	nextTurn = (len(stillIn)+ firstOut)%2
	if firstOut == 0:
		stillIn = stillIn [firstOut+1::2]
	else:
		stillIn = stillIn [0::2]
	firstOut = nextTurn

print(stillIn[0]+1)