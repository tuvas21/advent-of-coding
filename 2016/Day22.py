fname = 'Day22.inp'

useds = []
availables = []
totals = []
xs = []
ys = []

prev_x = 0
grid=[]

second_answer = 0

with open(fname) as f:
	f.readline()
	f.readline()
	row =[]
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		dat = content.split()
		file = dat[0].split('-')
		x = int(file[1][1:])
		y = int(file[2][1:])
		available = int(dat[3][:-1])
		used = int(dat[2][:-1])
		total = int(dat[1][:-1])
		
		if x != prev_x:
			grid.append(row)
			row =[]
			prev_x = x
		
		if used > 85 :
			state = 1
		elif used == 0:
			state = 2
			second_answer += x + (30-y)
		elif x == 33 and y==0:
			state = 3
			second_answer += 5 * 29
		else:
			state = 0
		
		
		row.append(state)
		
		
		xs.append(x)
		ys.append(y)
		useds.append(used)
		availables.append(available)
		totals.append(total)
	grid.append(row)
		
validPairs = 0
for i in range(len(useds)):
	if useds[i]==0:
		continue
	for j in range(len(useds)):
		if i==j:
			continue
		if availables[j]>=useds[i]:
			validPairs+=1
print(validPairs)
print(second_answer)

chars = '.#_G?'

for row in grid:
	print(''.join([chars[c] for c in row]))