inp = '.^^^^^.^^.^^^.^...^..^^.^.^..^^^^^^^^^^..^...^^.^..^^^^..^^^^...^.^.^^^^^^^^....^..^^^^^^.^^^.^^^.^^'

row = [c == '.' for c in inp]

ret = 0

for i in range(400000):
	if i == 40:
		print(ret)
	#print(''.join(['.' if c else '^' for c in row]))
	next = []
	ret += sum(row)
	for i in range(len(row)):
		c = row[i]
		if i>0:
			l = row[i-1]
		else:
			l = True
		if i < len(row)-1:
			r = row[i+1]
		else:
			r = True
		safe = True
		if not l and not c and r:
			safe = False
		elif not c and not r and l:
			safe = False
		elif not l and r and c:
			safe = False
		elif not r and l and c:
			safe = False
		next.append(safe)
	row = next
print(ret)	