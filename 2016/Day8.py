fname = 'Day8.inp'

grid = [[False,]*50 for i in range(6)]

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
			
		if content.startswith('rect'):
			dat = content[5:].split('x')
			for x in range(int(dat[0])):
				for y in range(int(dat[1])):
					grid[y][x]=True
		elif content.startswith('rotate row'):
			dat = content[13:].split(' by ')
			y = int(dat[0])
			x = int(dat[1])
			row = grid[y][:]
			for i in range(len(row)):
				grid[y][(x + i )%len(row)] = row[i]
			pass
		elif content.startswith('rotate column'):
			dat = content[16:].split(' by ')
			x = int(dat[0])
			y = int(dat[1])
			col = [row[x] for row in grid]
			for i in range(len(col)):
				grid[(y + i )%len(col)][x] = col[i]
		else:
			print("You Dummy, what did you do?!?!?!")
			
print(sum([sum(row) for row in grid]))
for row in grid:
	print(''.join(['█' if c else ' ' for c in row]))

			