fname = 'Day10.inp'

bots = [[] for i in range(1250)]
comparisons = [[] for i in range(250)]


with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		dat = content.split()
		if dat[0]=='bot':
			dat = content.split()
			a = int(dat[6])
			if dat[5]=='output':
				a= 1000 + a
			b = int(dat[11])
			if dat[10]=='output':
				b= 1000 + b
			comparisons[int(dat[1])] = (a,b)
		elif dat[0]=='value':
			dat = content.split()
			value = int(dat[1])
			bot = int(dat[5])
			
			bots[bot].append(value)

done = False
while not done:	
	done = True
	for i in range(250):
		bots[i].sort()
		if len(bots[i])==2:
			if bots[i][0]== 17 and bots[i][1] == 61:
				print(i)
			bots[comparisons[i][0]].append(bots[i][0])
			bots[comparisons[i][1]].append(bots[i][1])
			bots[i]=[]
			done = False

print(bots[1000][0]*bots[1001][0]*bots[1002][0])