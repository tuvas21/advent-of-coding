fname = 'Day3.inp'

ret = 0

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		vals = [int(n) for n in content.split()]
		vals.sort()
		if vals[0]+vals[1]>vals[2]:
			ret +=1
print(ret)

ret = 0
cvals = [[] for i in range(3)]

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		vals = [int(n) for n in content.split()]
		
		for i in range(3):
			cvals[i].append(vals[i])
			
		if len(cvals[0])==3:
			for vals in cvals:
				vals.sort()
		
				if vals[0]+vals[1]>vals[2]:
					ret +=1
			cvals = [[] for i in range(3)]
print(ret)