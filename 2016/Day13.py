fNum = 1352
#fNum = 10

maze = []

for y  in range(100):
	row = []
	for x in range(100):
		num = fNum + x*x + 3*x + 2*x*y + y + y*y
		bCount = bin(num).count('1')
		row.append(bCount % 2 == 1)
	maze.append(row)

#for row in maze:
#	print(''.join(['#' if c else '.' for c in row]))
		
visited = set([(1,1)])
testList = [(1,1)]
steps = 0

testLocs = [
( 0, 1),
( 1, 0),
( 0,-1),
(-1, 0)]

ox, oy = 31, 39

print(maze[oy][ox])

Found = False

while len(testList)>0 :
	nextList = []
	for next in testList:
		x, y = next
		if x == ox and y == oy:
			print("Path found: ",steps)
			Found = True
			
		for o in testLocs:
			xt = x + o[0]
			yt = y + o[1]
			if xt >= 0 and yt >= 0 and xt < len(maze[0]) and yt < len(maze) and not maze[yt][xt] and (xt,yt) not in visited:
				nextList.append((xt, yt))
				visited.update([(xt,yt)])
	testList = nextList
	steps +=1
	if steps == 50:
		print("Locations Visited: ",len(visited))