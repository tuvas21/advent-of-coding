fname = 'Day12.inp'

instructions=[]

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		dat = content.split()
		instructions.append(dat)

for c in [0,1]:
	reg=[0,0,c,0]
	instr = 0

	while instr < len(instructions):
		cmd = instructions[instr]
		
		if cmd[0]=='cpy':
			outi = ord(cmd[2])-ord('a')
			try:
				outv= int(cmd[1])
			except:
				outv = reg[ord(cmd[1])-ord('a')]
			reg[outi]=outv
			instr += 1
			continue
		regi = ord(cmd[1])-ord('a')
		if cmd[0]=='inc':
			reg[regi]+=1
		elif cmd[0]=='dec':
			reg[regi]-=1
		elif cmd[0]=='jnz':
			try:
				testv = int(cmd[1])
			except:
				testv = reg[regi]
			if testv != 0:
				instr += int(cmd[2])
				continue
		instr +=1
	print(reg)
			
	