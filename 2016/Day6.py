from collections import Counter

fname = 'Day6.inp'

dat = [Counter() for i in range(8)]

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
		for i in range(8):
			dat[i].update(content[i])

ret =''
for c in dat:
	ret+=c.most_common(1)[0][0]
print(ret)

ret =''
for c in dat:
	ret+=c.most_common()[-1][0]
print(ret)