from itertools import combinations

elevator = 0
generators = [['thulium', 'plutonium', 'strontium'],[],['promethium','ruthenium'],[]]
microchips = [['thulium', ],['plutonium', 'strontium'],['promethium','ruthenium'],[]]
generators = ['lst','','pr','']
microchips = ['t','ls','pr','']



#generators = [[],['hydrogen',],['lithium',],[]]
#microchips = [['hydrogen','lithium'],[],[],[]]

states = [(elevator,generators,microchips)]
steps = 0

tested = set()

while len(states)>0:
	next_states = []

	for state in states:
		microchips = state[2]
		generators = state[1]
		elevator = state[0]

		if len(microchips[3]) == 5 and len(generators[3]) ==5:
			print("Answer",steps)
			quit()
		temp_states = []
		for dir in [-1, 1]:
			if elevator + dir <0 or elevator + dir > 3:
				continue
			for o in generators[elevator]:
				temp_generators = [[c for c in g] for g in generators[:]]
				temp_generators[elevator] = [x for x in temp_generators[elevator] if x!=o]
				temp_generators[elevator+dir].append(o)
				temp_generators[elevator].sort()
				temp_generators[elevator+dir].sort()
				temp_states.append((elevator+dir,[''.join(x) for x in temp_generators],microchips[:]))
			if len(generators[elevator])>1:
				for o in combinations(generators[elevator],2):
					temp_generators = [[c for c in g] for g in generators[:]]
					temp_generators[elevator] = [x for x in temp_generators[elevator] if x not in o]
					temp_generators[elevator+dir]+=o
					temp_generators[elevator].sort()
					temp_generators[elevator+dir].sort()
					temp_states.append((elevator+dir,[''.join(x) for x in temp_generators],microchips[:]))
			for o in microchips[elevator]:
				temp_microchips = [[c for c in g] for g in microchips[:]]
				temp_microchips[elevator] = [x for x in temp_microchips[elevator] if x!=o]
				temp_microchips[elevator+dir].append(o)
				temp_microchips[elevator].sort()
				temp_microchips[elevator+dir].sort()
				temp_states.append((elevator+dir,generators[:],[''.join(x) for x in temp_microchips]))
			if len(microchips[elevator])>1:
				for o in combinations(microchips[elevator],2):
					temp_microchips = [[c for c in g] for g in microchips[:]]
					temp_microchips[elevator] = [x for x in temp_microchips[elevator] if x not in o]
					temp_microchips[elevator+dir]+=o
					temp_microchips[elevator].sort()
					temp_microchips[elevator+dir].sort()
					temp_states.append((elevator+dir,generators[:],[''.join(x) for x in temp_microchips]))
			for o in microchips[elevator]:
				if o in generators[elevator]:
					temp_microchips = [[c for c in g] for g in microchips[:]]
					temp_microchips[elevator] = [x for x in temp_microchips[elevator] if x!=o]
					temp_microchips[elevator+dir].append(o)
					temp_microchips[elevator].sort()
					temp_microchips[elevator+dir].sort()
					temp_generators = [[c for c in g] for g in generators[:]]
					temp_generators[elevator] = [x for x in temp_generators[elevator] if x!=o]
					temp_generators[elevator+dir].append(o)
					temp_generators[elevator].sort()
					temp_generators[elevator+dir].sort()
					
					temp_states.append((elevator+dir,[''.join(x) for x in temp_generators],[''.join(x) for x in temp_microchips]))
					
		for s in temp_states:
			valid = True
			for i in range(4):
				if valid and len(s[1][i])>0:
					for x in s[2][i]:
						if x not in s[1][i]:
							valid=False
							break
			hash = "%i%s/%s"%(s[0],'-'.join(s[1]),'@'.join(s[2]))
			if valid and hash not in tested:
				next_states.append(s)
				tested.update([hash])
			
	states = next_states

	steps += 1
	print(steps,len(states))
	
print(steps)