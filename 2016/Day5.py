import hashlib

inp = 'reyedfim'


code = ''
fancy_code = ['',]*8
ret = 0
while any([f=='' for f in fancy_code]):
	m = hashlib.md5()
	test = inp + str(ret)
	m.update(test.encode('ASCII'))
	outhash = m.hexdigest()
	if outhash[:5]=='00000':
		print(ret,outhash[5],outhash)
		code += outhash[5]
		try:
			index = int(outhash[5])
			if index <8:
				if fancy_code[index]=='':
					fancy_code[index] = outhash[6]
					print(fancy_code)
		except:
			pass
	ret += 1
print(code[:8])
print(''.join(fancy_code))

