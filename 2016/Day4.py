from collections import Counter

fname = 'Day4.inp'

ret = 0

base_string='abcdefghijklmnopqrstuvwxyz'

with open(fname) as f:
	while(True):
		content = f.readline().strip()
		if len(content)==0:
			break;
			
		c = Counter(base_string)
		dat = content.split('-')
		for d in dat[:-1]:
			c.update(d)
		checksum = dat[-1][-6:-1]
		true_checksum = ''.join([k[0] for k in c.most_common(5)])
		if checksum == true_checksum:
			sector = int(dat[-1][:-7])
			ret += sector
			outString = ''
			for d in dat[:-1]:
				for ch in d:
					ct = ord(ch) - ord('a')
					clt = chr((ct+ sector) % 26 + ord('a'))
					outString+= clt
				outString+= ' '
			print(sector,outString)
print(ret)