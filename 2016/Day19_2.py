inp = 3018458

#inp = 128

stillIn = [i+1 for i in range(inp)]
turn = 0
looped = False
removedValues = set()

maxRemove = len(stillIn)

while len(stillIn)>1:
	if not looped:
		removed = turn + (len(stillIn)+ len(removedValues))//2 
		if removed >= len(stillIn):
			looped = True
			stillIn = [x for x in stillIn if x not in removedValues]
			print("Cheated: Removed ",len(removedValues))
			#print(stillIn)
			removedValues = set()
			maxRemove = stillIn[turn-1]
			continue
		else:
			removedValues.update([stillIn[removed]])
	if looped:
		removed = turn + (-len(stillIn)+ len(removedValues))//2
		removedValue = stillIn[removed]
		#print(removed,stillIn[turn],removedValue)
		if removedValue>maxRemove:
			stillIn = [x for x in stillIn if x not in removedValues]
			if len(removedValues)>0:
				print("More cheating: ",len(removedValues))
			turn = turn - len(removedValues)
			removedValues = set()
			maxRemove = stillIn[turn-1]
			
			removed = (turn + len(stillIn)//2) % len(stillIn)
			del stillIn[removed]
			turn -= 1
		else:
			removedValues.update([removedValue])
	turn += 1
	if turn >= len(stillIn):
		if len(removedValues)>0:
			print("Even more cheating: ",len(removedValues))
		stillIn = [x for x in stillIn if x not in removedValues]
		turn = 0
		looped = len(stillIn)<3
		removedValues = set()
		maxRemove = len(stillIn)
		print("Still remaining:", len(stillIn))

print(stillIn[0])
