test=[1,2,1,2,1,2,1,2,3,2,3,4,3,2,3,4,3,2,3,4,3,2,3,4,5,4,3,4,5,6,5]

	
from math import sqrt

def calcValue(input):
	nSquare=int(sqrt(input-1))
	if nSquare%2==0:
		nSquare-=1
	rem=(input-(nSquare+1)//2-((nSquare)*(nSquare)))
	adj=rem%((nSquare+1))
	if (adj>(nSquare+1)//2):
		#print("  %i %i"%(rem,nSquare))
		adj=nSquare+1-adj
	output=(nSquare+1)//2+adj
	#print (input,nSquare,adj,output,test[input-2])
	return output

input=347991
print(calcValue(input))
#for i in range(len(test)):
#	input=i+2
#	output=calcValue(input)
	#print(input,output,test[i])
	#print ("%i %i %i %i %i"%(input,test[i],nSquare,adj,output))

	
def loc(index):
	nSquare=int(sqrt(index-1))
	if nSquare%2==0:
		nSquare-=1
	shell=(nSquare+1)//2
	rem=(index-1-((nSquare)*(nSquare)))
	adj=rem%(nSquare+1)
	seg=rem//(nSquare+1)
	if seg==0:
		return [shell,adj-shell+1]
	if seg==1:
		return [shell-1-adj,shell]
	if seg==2:
		return [-shell,shell-adj-1]
	if seg==3:
		return [adj-shell+1,-shell]

	return [nSquare, shell, adj, seg]

def revLoc(loc):
	shell=max([abs(i) for i in loc])
	nSquare=shell*2-1
	if loc[1]==-shell:
		return nSquare*nSquare+7*shell+loc[0]
	if loc[0]==-shell:
		return nSquare*nSquare+5*shell-loc[1]
	if loc[1]==shell:
		return nSquare*nSquare+3*shell-loc[0]
	else:
		return nSquare*nSquare+shell+loc[1]
	return shell
	
vals=[1]
index=2

testDiff=[[-1,-1],[-0,-1],[1,-1],[1,0],[1,1],[0,1],[-1,1],[-1,0]]

while (True):
	l=loc(index)
	
	neighbors=[revLoc([d[0]+l[0],d[1]+l[1]]) for d in testDiff]
	nVal=sum([vals[i-1] for i in neighbors if i<index])
	print(index,nVal,neighbors)
	
	if nVal>input:
		break;
	vals+=[nVal]
	index+=1
	