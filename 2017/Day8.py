fname="Day8.inp"

reg={}

def r(register):
	if register in reg:
		return reg[register]
	return 0
	
def test(value, test, comparitor):
	if test == "<":
		return value < comparitor
	if test == "<=":
		return value <= comparitor
	if test == "==":
		return value == comparitor
	if test == "!=":
		return value != comparitor
	if test == ">":
		return value > comparitor
	if test == ">=":
		return value >= comparitor
	print (test)

mv=0
with open(fname) as f:
	while(True):
		content = f.readline()
		if content=='':
			break;
		sp=content.split()
		if test(r(sp[4]),sp[5],int(sp[6])):
			if sp[1]=="inc":
				reg[sp[0]]=r(sp[0])+int(sp[2])
			else:
				reg[sp[0]]=r(sp[0])-int(sp[2])
			if mv<reg[sp[0]]:
				mv=reg[sp[0]]
print (mv)
mv=0
v=''
for r in reg:
	if reg[r]>mv:
		v=r
		mv=reg[r]
print(v,mv)