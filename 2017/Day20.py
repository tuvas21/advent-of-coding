import string
fname="Day20.inp"

p=[]
v=[]
a=[]
rtime=2503
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		sp=content.split()
		p+=[[int(i) for i in sp[0][3:-2].split(',')]]
		v+=[[int(i) for i in sp[1][3:-2].split(',')]]
		a+=[[int(i) for i in sp[2][3:-1].split(',')]]

ci=[]
for i in range(len(p)):
	if p[i] in p[0:i] or p[i] in p[i+1:]:
		ci+=[i]
ci.reverse()
for i in ci:
	p.pop(i)
	a.pop(i)
	v.pop(i)
if len(ci)>0:
	print(t,len(p))

for t in range(100):
	ci=[]
	for i in range(len(p)):
		#print(i,t,p[i],v[i],a[i])
		for j in range(3):
			v[i][j]+=a[i][j]
			p[i][j]+=v[i][j]
			if p[i] in p[0:i] or p[i] in p[i+1:]:
				for n in range(len(p)):
					if p[n]==p[i]:
						ci+=[n]
		#print(i,t,p[i],v[i],a[i])
	ci=sorted(list(set(ci)))
	ci.reverse()
	#print(ci)
	for i in ci:
		#print(i)
		p.pop(i)
		a.pop(i)
		v.pop(i)
	if len(ci)>0:
		print(t,len(p))
			
mn=1000000000
idx=0
for i in range(len(p)):
	d=sum([abs(a) for a in p[i]])
	if d<mn:
		mn=d
		idx=i

print(mn,idx)
print(len(a))