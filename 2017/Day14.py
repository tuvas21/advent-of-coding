import binascii

input='ffayrhll'

#input='flqrgnkx'

#input=[3,4,1,5]
#cList=[i for i in range(5)]

def rev(i,s,n,m):
	if i<s:
		if i>(s+n-1)%m or s+n<=m:
			#print("Normal %i"%i)
			return i
	elif i>s+n-1:
		#print("Normal %i"%i)
		return i
	rotIndex=(n+s-(i-s)-1+m)%m
	return rotIndex
		
		

def knotHash(inString):
	cList=[i for i in range(256)]
	vals=[ord(c) for c in inString]+[17, 31, 73, 47, 23]
	cPosition=0

	skipSize=0

	listSize=len(cList)

	for i in range(64):
		for n in vals:
			#print(cPosition, n,listSize)
			cList=[cList[rev(i, cPosition, n,listSize)] for i in range(listSize)]
			cPosition+=(n+skipSize)
			cPosition%=listSize
			#print (cList,cPosition,n)
			skipSize+=1
			
	ret=''
	for i in range(16):
		v=cList[i*16]
		for j in range(1,16):
			v=v^cList[i*16+j]
		ret+='%02x'%v
		#print('%02x'%v)
	return ret
	
squares=0
grid=[]
gridSize=128
for i in range(gridSize):
	val=knotHash("%s-%i"%(input,i))
	binary_string = format(int(val,16), '#0130b')
	#print(len(binary_string),binary_string)
	squares+=sum([c=='1' for c in binary_string[2:]])
	grid+=[[c=='1' for c in binary_string[2:]]]
print(squares)


#grid=[
#	[False, True,True, True],
#	[False,True, False, True],
#	[False, True,True,True],
#	[True,False,False,False]]

regions=0
table={}
regionNumber=[]
nextRegion=1
for ri in range(gridSize):
	rowRegion=[0]*gridSize
	for ci in range(gridSize):
		if grid[ri][ci]:
			region=-1
			if ci>0 and rowRegion[ci-1]>0:
				region=rowRegion[ci-1]
				oregion=region
				while region in table:
					region=table[region]
					if region==oregion:
						print("Loop detected for %i"%region)
						break
			if ri>0:
				if regionNumber[ri-1][ci]>0:
					if region>0 and region!=regionNumber[ri-1][ci]:
						colRegion=regionNumber[ri-1][ci]
						while colRegion in table:
							colRegion=table[colRegion]
						mn=min(region,colRegion)
						mx=max(region,colRegion)
						if mn in table:
							print("Already had %i"%mn)
						if mn==mx:
							print("Loop detected, no need to add table row")
						else:
							table[mn]=mx
					else:
						region=regionNumber[ri-1][ci]
				oregion=region
				while region in table:
					region=table[region]
					if region==oregion:
						print("Loop detected for %i"%region)
						break
			if region==-1:
				region=nextRegion
				nextRegion+=1
			rowRegion[ci]=region
	regionNumber+=[rowRegion]
	
outputs=[i for i in range(1,nextRegion)]
for k in table:
	for i in range(len(outputs)):
		if outputs[i]==k:
			outputs[i]=table[k]
print(nextRegion,len(set(outputs)),len(table))
#print (outputs)
#print(len(set(outputs)))
print(nextRegion-1-len(table))