import string
fname="Day19.inp"

input=[]
rtime=2503
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		input+=[content]
		
y=0
x=input[0].find("|")
d='s'

ym=len(input)
xm=len(input[0])

ret=''
steps=0

while( x>=0 and y>=0):
	steps+=1
	if d=='s':
		y+=1
	elif d=='n':
		y-=1
	elif d=='e':
		x+=1
	elif d=='w':
		x-=1
	c=input[y][x]
	#print(x,y,c)
	if c==' ':
		break
	elif c in string.ascii_uppercase:
		ret+=c
	elif c=='+':
		if d=='e' or d=='w':
			if y<ym-1 and input[y+1][x] !=' ':
				d='s'
			if y>0 and input[y-1][x] !=' ':
				d='n'
		elif d=='n' or d=='s':
			if x<xm-1 and input[y][x+1] !=' ':
				d='e'
			if x>0 and input[y][x-1] !=' ':
				d='w'
		#print(d)
		
print(ret)
print(steps)