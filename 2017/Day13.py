fname="Day13.inp"

depth=[0]*97

with open(fname) as f:
	while(True):
		content = f.readline()
		if content=='':
			break
		sp=content.split()
		depth[int(sp[0][:-1])]=int(sp[1])
	
	
delay=0
found=True

while(found):
	severity=0
	found=False

	for i in range(delay,97+delay):
		#print(i,depth[i],i%(2*(depth[i-delay]-1)))
		if i>=delay and depth[i-delay]>0 and i%(2*(depth[i-delay]-1))==0:
			severity+=i*depth[i-delay]
			found=True
	if (delay==0):
		print(severity)
	delay+=1
	#break
print(delay-1)