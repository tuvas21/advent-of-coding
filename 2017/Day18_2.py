fname="Day18.inp"

input=[]
rtime=2503
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		input+=[content.split()]	
		
def tick(reg,i,f):
	s=[]
	r=ord(input[i][1])-ord('a')
	comm=input[i]
	waiting=False
	if input[i][0]=="snd":
		try:
			s=[int(input[i][1])]
		except:
			s=[reg[r]]
	elif input[i][0]=="set":
		try:
			reg[r]=int(input[i][2])
		except:
			reg[r]=reg[ord(input[i][2])-ord('a')]
	elif input[i][0]=="add":
		try:
			reg[r]=reg[r]+int(input[i][2])
		except:
			reg[r]=reg[r]+reg[ord(input[i][2])-ord('a')]
	elif input[i][0]=="mul":
		try:
			reg[r]=reg[r]*int(input[i][2])
		except:
			reg[r]=reg[r]*reg[ord(input[i][2])-ord('a')]
	elif input[i][0]=="mod":
		try:
			reg[r]=reg[r]%int(input[i][2])
		except:
			reg[r]=reg[r]%reg[ord(input[i][2])-ord('a')]
	elif input[i][0]=="rcv":
		if len(f)>0:
			reg[r]=f[0]
			f=f[1:]
			#print(f)
		else:
			waiting=True
	elif input[i][0]=="jgz":
		#print(i,r,reg,input[i])
		jump=True
		try:
			jump=int(input[i][1])>0
		except:
			jump=reg[r]>0
		if jump:
			try:
				i+=int(input[i][2])-1
			except:
				i+=reg[ord(input[i][2])-ord('a')]-1
	if not waiting:
		i+=1
	#print(i,reg)
	return reg,f,i,s,waiting
			
reg0=[0]*26
reg0[ord('p')-ord('a')]=0
f0=[]
i0=0
w0=False

reg1=[0]*26
reg1[ord('p')-ord('a')]=1
f1=[]
i1=0
w1=False

ret=0

while not (w1 and w0):
	reg0,f0,i0,s0,w0=tick(reg0,i0,f0)
	reg1,f1,i1,s1,w1=tick(reg1,i1,f1)
	
	if len(s1)>0:
		#print(s0+f1)
		ret+=1
	f0+=s1
	f1+=s0
	
print(i0,reg0)
print(i1,reg1)
print(ret)
	
