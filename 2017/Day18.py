fname="Day18.inp"

input=[]
rtime=2503
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		input+=[content.split()]	
			
reg=[0]*26
f=0
i=0
s=0
while i>=0 and i<len(input):
	r=ord(input[i][1])-ord('a')
	if input[i][0]=="snd":
		f=reg[r]
	elif input[i][0]=="set":
		try:
			reg[r]=int(input[i][2])
		except:
			reg[r]=reg[ord(input[i][2])-ord('a')]
	elif input[i][0]=="add":
		try:
			reg[r]=reg[r]+int(input[i][2])
		except:
			reg[r]=reg[r]+reg[ord(input[i][2])-ord('a')]
	elif input[i][0]=="mul":
		try:
			reg[r]=reg[r]*int(input[i][2])
		except:
			reg[r]=reg[r]*reg[ord(input[i][2])-ord('a')]
	elif input[i][0]=="mod":
		try:
			reg[r]=reg[r]%int(input[i][2])
		except:
			reg[r]=reg[r]%reg[ord(input[i][2])-ord('a')]
	elif input[i][0]=="rcv":
		if f>0:
			reg[r]=f
			print(f)
	elif input[i][0]=="jgz":
		if reg[r]>0:
			try:
				i+=int(input[i][2])-1
			except:
				i+=reg[ord(input[i][2])-ord('a')]-1
	else:
		print(input[i])
	i+=1
	print(i,reg)