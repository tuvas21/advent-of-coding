fname="Day22.inp"

input=[]
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		input+=[content]
		

gridBorder=200
		
		
gridSize=len(input)+2*gridBorder
grid=[]

#print(input)

for y in range(0,gridSize):
	row=[0]*gridSize
	for x in range(0,gridSize):
		if x>=gridBorder and x<gridBorder+len(input) and y>=gridBorder and y<gridBorder+len(input):
			row[x]= 2 if input[y-gridBorder][x-gridBorder] == '#' else 0
			#print(x,y,input[x-gridBorder][y-gridBorder])
	grid+=[row]
#	print(y,row)

#print(grid)

x=gridSize//2
y=gridSize//2

dir=2

states='.W#F'

def printGrid(grid):
	for r in grid:
		row=''
		for c in r:
			row+=states[c]
		print(row)

numInfected=0
for i in range(10000000):
	if grid[y][x]==0:
		dir=(dir+1)%4
	elif grid[y][x]==2:
		dir=(dir+3)%4
	elif grid[y][x]==3:
		dir=(dir+2)%4
	grid[y][x]=(grid[y][x]+1)%4
	numInfected+= grid[y][x]==2
	
		
	if dir==0:
		y+=1
	elif dir==1:
		x+=1
	elif dir==2:
		y-=1
	elif dir==3:
		x-=1
	
	if x>=gridSize or y>=gridSize or x<0 or y<0:
		print("Out of bounds!",x,y,i)
		
#	print(x,y)
#	printGrid(grid)
print(numInfected)

#print(grid[y][x],grid[y-1][x+1])