import string

fname="Day16.inp"

with open(fname) as f:
	while(True):
		input = f.readline()
		break

startVal=string.ascii_lowercase[0:16]
		
loc=[c for c in startVal]

count=0

for iter in range(1000000000):
	count+=1
	if count==0:
		print(''.join(loc))
		break;
	for c in input.split(','):
		if c[0]=='s':
			s=int(c[1:])
			loc=loc[-s:]+loc[:-s]
		if c[0]=='x':
			a,b=c[1:].split('/')
			loc[int(a)],loc[int(b)]=loc[int(b)],loc[int(a)]
		if c[0]=='p':
			a,b=c[1:].split('/')
			i=loc.index(a.lower())
			j=loc.index(b.lower())
			loc[i],loc[j]=loc[j],loc[i]
	if (i==0):
		print(''.join(loc))
	if ''.join(loc)==startVal:
		print(iter)
		count=-(1000000000%(iter+1))-1

	