fname="Day24.inp"

input=[]
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		input+=[sorted([int(i) for i in content.split('/')])]
print(input)

def findBestBridge(inputs,openConnector,usedIndexes=[],strength=0):
	best=0
	length=0
	maxStrength=strength
	for i in range(len(inputs)):
		if i in usedIndexes:
			continue
		c=inputs[i]
		if c[0]==openConnector or c[1]==openConnector:
			nc=c[1] if c[0]==openConnector else c[0]
			l,s=findBestBridge(inputs,nc,usedIndexes+[i],sum(c)+strength)
			#print(usedIndexes+[i],l,s)
			if l>length:
				length=l
				maxStrength=s
			elif l==length and s>strength:
				maxStrength=s
	return max(length,len(usedIndexes)),maxStrength
	
print(findBestBridge(input,0))