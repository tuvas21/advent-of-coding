import math
fname="Day21.inp"

rules={}

#Todo Split grids of 4 into 4 grids of 2
def convertToGrid(inpstring):
	sp=inpstring.split('/')
	grid=[]
	for r in sp:
		row=[]
		for c in r:
			row+=[c=="#"]
		grid+=[row]
	return grid
	
#Todo Split grids of 4 into 4 grids of 2
def combineGrids(grids):
	#print("combine Input",grids)
	factor=math.floor(math.sqrt(len(grids)))
	gridSize=len(grids[0])
	#print("combine",len(grids),factor,gridSize)
	ret=[]
	for x in range(factor*len(grids[0])):
		row=[]
		for y in range(factor*len(grids[0])):
			gridNumber=x//gridSize+(y//gridSize)*factor
			row+=[grids[gridNumber][y%gridSize][x%gridSize]]
		ret+=[row]
	#print("combine Output",ret)
	return ret
	
def splitGrid(grid,newSize):
	grids=[]
	for mx in range(0,len(grid),newSize):
		for my in range(0,len(grid),newSize):
			newGrid=[]
			for x in range(newSize):
				row=[]
				for y in range(newSize):
					row+=[grid[my+y][mx+x]]
				newGrid+=[row]
			grids+=[newGrid]
	return grids
	
rot3=  [[[0,1,2],[0,1,2]],
		[[0,1,2],[2,1,0]],
		[[2,1,0],[0,1,2]],
		[[2,1,0],[2,1,0]]]
rot2=  [[[0,1],[0,1]],
		[[0,1],[1,0]],
		[[1,0],[0,1]],
		[[1,0],[1,0]]]

def isEqual(inpString,testGrid):
	ig=convertToGrid(inpString)
	if len(ig)!=len(testGrid):
		return False
	if len(ig)==2:
		rot=rot2
	else:
		rot=rot3
	match=[True]*8
#	print(testGrid)
#	print(ig)
	for ri in range(len(ig)):
		for ci in range(len(ig)):
			for t in range(len(rot)):
				#print(ri,ci,rot[t][0][ri],rot[t][1][ci])
				match[t]&=ig[ri][ci]==testGrid[rot[t][0][ri]][rot[t][1][ci]]
				match[t+4]&=ig[ci][ri]==testGrid[rot[t][0][ri]][rot[t][1][ci]]
				#break
	#print(match)
	return any(match)
			
			

state=[[False,True,False],[False,False,True],[True,True,True]]

with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		sp=content.split()

		rules[sp[0]]=convertToGrid(sp[2])

#print(rules)

def numOn(grid):
	ret=0
	#print(grid)
	for r in grid:
		#print(r)
		ret+=sum(r)
	return ret

for i in range(18):
	next=[]
	if len(state)%2==0:
		grids=splitGrid(state,2)
	else:
		grids=splitGrid(state,3)
	#print(grids)
	for grid in grids:
		#print(grid)
		found=False
		for ri in rules:
			if isEqual(ri,grid):
				next+=[rules[ri]]
				if (found):
					print("Two rules found")
					print(grid)
				found=True
				#print(ri,grid,rules[ri])
				break
		if not found:
			print("Could not find a rule for grid")
			print(grid)
	state=combineGrids(next)
	print(i+1,numOn(state))
