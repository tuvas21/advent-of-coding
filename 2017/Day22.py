fname="Day22.inp"

input=[]
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		input+=[content]
		

gridBorder=200
		
		
gridSize=len(input)+2*gridBorder
grid=[]

#print(input)

for y in range(0,gridSize):
	row=[False]*gridSize
	for x in range(0,gridSize):
		if x>=gridBorder and x<gridBorder+len(input) and y>=gridBorder and y<gridBorder+len(input):
			row[x]=input[y-gridBorder][x-gridBorder] =='#'
			#print(x,y,input[x-gridBorder][y-gridBorder])
	grid+=[row]
#	print(y,row)

#print(grid)

x=gridSize//2
y=gridSize//2

dir=2

def printGrid(grid):
	for r in grid:
		row=''
		for c in r:
			row+='#' if c else '.'
		print(row)

numInfected=0
for i in range(10000):
	if grid[y][x]:
		dir=(dir+3)%4
	else:
		dir=(dir+1)%4
	grid[y][x]=not grid[y][x]
	numInfected+= grid[y][x]
	
		
	if dir==0:
		y+=1
	elif dir==1:
		x+=1
	elif dir==2:
		y-=1
	elif dir==3:
		x-=1
	
	if x>=gridSize or y>=gridSize or x<0 or y<0:
		print("Out of bounds!",x,y,i)
		
#print(x,y)
#printGrid(grid)
print(numInfected)

#print(grid[y][x],grid[y-1][x+1])