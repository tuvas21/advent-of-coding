fname="Day12.inp"

conn={}
keys=[]

with open(fname) as f:
	while(True):
		content = f.readline()
		if content=='':
			break
		sp=content.split()
		conn[int(sp[0])]=[int(n[:-1]) if n[-1]==',' else int(n) for n in sp[2:]]
		keys+=[int(sp[0])]
		#print(conn[int(sp[0])])
		
def findConnected(val,connected=[]):
	#print(connected)
	if val in connected:
		return []
	connected+=[val]
	for i in conn[val]:
		connected+=findConnected(i,connected)
	return set(connected)

print(len(findConnected(0)))

groups=0

connected=[]
while len(keys)>0:
	testKey=-1
	for k in keys:
		if k not in connected:
			testKey=k
			break
	if testKey==-1:
		break
	newConnected=findConnected(testKey,[])
	#print(len(newConnected),testKey)
	connected+=newConnected
	groups+=1

print(groups)