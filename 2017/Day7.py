fname="Day7.inp"

weight={}
children={}
with open(fname) as f:
	while(True):
		content = f.readline()
		if content=='':
			break;
		sp=content.split()
		weight[sp[0]]=int(sp[1][1:-1])
		if len(sp)>2:
			children[sp[0]]=[sp2.split(',')[0] for sp2 in sp[3:]]
			#print (sp[3].split(','))
			
for p in children:
	found=True
	for p2 in children:
		for c in children[p2]:
			if p==c:
				found=False
				break
	if found:
		print (p)
		
def w(p):
	ret=weight[p]
	if p in children:
		for c in children[p]:
			ret+=w(c)
	return ret

for p in children:
	weights=[w(c) for c in children[p]]
	if not all([a==weights[0] for a in weights]):
		print (p,weights,children[p])
#Note: This doesn't give the solution, but should give the information to find the solution. The key is to figure out which is the problem child, and what the weight should be to make it balance.
#This gives the information that is required
	