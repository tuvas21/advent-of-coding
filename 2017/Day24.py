fname="Day24.inp"

input=[]
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		input+=[sorted([int(i) for i in content.split('/')])]
print(input)

def findBestBridge(inputs,openConnector,usedIndexes=[]):
	best=0
	for i in range(len(inputs)):
		if i in usedIndexes:
			continue
		c=inputs[i]
		if c[0]==openConnector or c[1]==openConnector:
			nc=c[1] if c[0]==openConnector else c[0]
			best=max(sum(c)+findBestBridge(inputs,nc,usedIndexes+[i]),best)
	return best
	
print(findBestBridge(input,0))