fname="Day11.inp"

a=0
b=0
c=0

with open(fname) as f:
	while(True):
		content = f.readline()
		input=content.split(',')
		break
		
maxD=0
for i in input:
	if i=="nw":
		a+=1
	elif i=="se":
		a-=1
	elif i=="n":
		b+=1
	elif i=="s":
		b-=1
	elif i=="ne":
		c+=1
	elif i=="sw":
		c-=1
	else:
		print("Could not manage "+i)
		
	v=sorted([a,b,c])
	if maxD<abs(v[2])+abs(v[1]):
		maxD=abs(v[2])+abs(v[1])
		#print(v)
print(a,b,c)
print(maxD)