import re

fname = 'Day7.txt'

num = re.compile(' [0-9]+ ')

def parseRow(data):
	sdata = data.split('bag')
	ret = []
	if 'no other' not in sdata[1]:
		for row in sdata[1:]:
			color = num.split(row)[-1].strip()
			if len(color)>2:
				ret.append((color,int(num.findall(row)[0])))
	return {sdata[0].strip():ret}

def numBags(bag,dat):
	print(bag,dat[bag])
	return sum([a[1]* (1+ numBags(a[0],dat)) for a in dat[bag]])

dat = {}
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		pd = parseRow(content.strip())
		for k in pd:
			dat[k] = pd[k]

types = set(['shiny gold'])

found_types = set()
done = False
while not done:
	done = True
	for k in dat:
		if any([a[0] in types and a[1]>0 for a in dat[k]]):
			if k not in found_types:
				found_types |= set([k])
				done = False
				types |= set([k])

print (len(found_types))
print (numBags('shiny gold',dat))