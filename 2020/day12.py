from math import cos, sin, pi

fname = 'day12.txt'

dat = []

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append(content.strip())
		
		
dir = 0
x = 0
y = 0
for i in dat:
	c = i[0]
	n = int(i[1:])
	if c == 'N':
		y += n
	elif c =='S':
		y -= n
	elif c == 'E':
		x += n
	elif c == 'W':
		x -= n
	elif c == 'L':
		dir = (dir + n) % 360
	elif c == 'R':
		dir = (dir - n) % 360
	elif c == 'F':
		x += n * int(cos(dir*pi/180))
		y += n * int(sin(dir*pi/180))
	
print(x,y,abs(x)+abs(y))

dir = 0
x = 0
y = 0
wx = 10
wy = 1
for i in dat:
	c = i[0]
	n = int(i[1:])
	if c == 'N':
		wy += n
	elif c =='S':
		wy -= n
	elif c == 'E':
		wx += n
	elif c == 'W':
		wx -= n
	elif c == 'L':
		tx,ty = wx,wy
		if n == 270:
			wx = ty
			wy = - tx
		elif n == 180:
			wx = -tx
			wy = -ty
		elif n == 90:
			wx = -ty
			wy = tx
	elif c == 'R':
		tx,ty = wx,wy
		if n == 90:
			wx = ty
			wy = - tx
		elif n == 180:
			wx = -tx
			wy = -ty
		elif n == 270:
			wx = -ty
			wy = tx
	elif c == 'F':
		x += n * wx
		y += n * wy
		
	#print(x,y,wx,wy)
	
print(x,y,abs(x)+abs(y))