fname = 'day16.txt'

data = {}
tickets = []

mode = 0

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		c = content.strip()
		if c == '':
			continue
		elif c == 'your ticket:':
			mode = 1
		elif c == 'nearby tickets:':
			mode = 2
		elif mode == 0:
			s = content.split(':')
			d = s[1].strip().split(' or' )
			data[s[0]] = [[int(a) for a in b.split('-')] for b in d]
		elif mode == 1:
			myTicket = [int(a) for a in c.split(',')]
		elif mode == 2:
			tickets.append([int(a) for a in c.split(',')])
			
count = 0
validList = []
for t in tickets:
	valid = True
	for n in t:
		inPair = False
		for k in data:
			for p in data[k]:
				if p[0] <= n and p[1] >= n:
					inPair = True
					break;
		if not inPair:
			valid = False
			count += n
	if valid:
		validList.append(t)
print(count)


possibles = {}
for k in data:
	found = []
	for i in range(len(validList[0])):
		valid = True
		for t in validList:
			inPair = False
			for p in data[k]:
				n = t[i]
				if p[0] <= n and p[1] >= n:
					inPair = True
					break;
			if not inPair:
				valid = False
				break
		if valid:
			found.append(i)

	possibles[k]= found
	
ret = 1
while any([len(possibles[k])>0 for k in possibles]):
	for i in range(len(validList[0])):
		valid = []
		for k in possibles:
			if i in possibles[k]:
				valid.append(k)
		if len(valid) == 1:
			print (valid[0],i)
			if 'departure' in valid[0]:
				ret *= myTicket[i]
			possibles[valid[0]] = []
print(ret)

		