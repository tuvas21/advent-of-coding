fname = 'Day5.txt'

dat = []

def seatDecoder(seat):
	row = 0
	for c in seat[:7]:
		row *= 2
		if c=='B':
			row += 1
	column = 0
	for c in seat[7:]:
		column *= 2
		if c=='R':
			column += 1
	return (row,column,row*8+column)

maxSeat = 0

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append(seatDecoder(content.strip())[2])

mx = max(dat)
mn = min(dat)

print(mx)
for n in range(mn,mx):
	if n not in dat:
		print (n)
