fname = 'day11.txt'

dat = []

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append([c for c in content.strip()])

diff=True
while diff:
	diff=False
	prev = [row[:] for row in dat[:]]
	for i in range(len(dat)):
		for j in range(len(dat[0])):
			seat = prev[i][j]
			if seat!='.':
				count = 0
				for dir in [(1,1),(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1)]:
					x = i + dir[0]
					y = j + dir[1]
					while x >= 0 and x < len(prev) and y >=0 and y < len(prev[0]):
						if prev[x][y] == '.':
							x = x + dir[0]
							y = y + dir[1]
							continue
						else:
							if prev[x][y] == '#':
								count += 1
							break
				if seat == 'L' and count == 0:
					dat[i][j]='#'
					diff=True
				elif seat == '#' and count >= 5:
					dat[i][j]='L'
					diff=True

print(sum([sum([c=='#' for c in row]) for row in dat]))
