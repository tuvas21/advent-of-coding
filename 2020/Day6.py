fname = 'Day6.txt'
#Must have a space at the end

dat = []
temp = []

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		if len(content)==1:
			dat.append(temp)
			temp=[]
		else:
			temp.append(content.strip())
	dat.append(temp)
	

ret1 = 0
for g in dat:
	a = set()
	for r in g:
		a|=set(r[:])
	ret1+=len(a)

print(ret1)

ret2 = 0
for g in dat:
	a = set(g[0][:])
	for r in g:
		a&=set(r[:])
	ret2+=len(a)
print(ret2)