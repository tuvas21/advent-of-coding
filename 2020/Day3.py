fname = 'Day3.txt'

dat = []

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append(content.strip())

width = len(dat[0])


out = 1
for slope in [(1,1),(3,1),(5,1),(7,1),(1,2)]:
	trees = 0
	loc = 0
	vl = -1
	
	for row in dat:
		vl += 1
		if vl%slope[1] !=0:
			continue
		if row[loc] == '#':
			trees+=1
		loc = (loc + slope[0]) % width
	print(trees)
	out *= trees
print(out)

