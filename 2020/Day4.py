fname = 'Day4.txt'

dat = []


#    byr (Birth Year)
#    iyr (Issue Year)
#    eyr (Expiration Year)
#    hgt (Height)
#    hcl (Hair Color)
#    ecl (Eye Color)
#    pid (Passport ID)
#    cid (Country ID)

def isHex(num):
	for c in num:
		if c not in '0123456789abcdef':
			return False
	return True

def valid(set):
	for key in ['byr','iyr','eyr','hgt','hcl','ecl','pid']:
		if key not in set:
			return False
	return True
	
def valid2(set):
	try:
		year = int(set['byr'])
		if year<1920 or year > 2002:
			return False
		year = int(set['iyr'])
		if year<2010 or year > 2020:
			return False
		year = int(set['eyr'])
		if year<2020 or year > 2030:
			return False
		if set['hgt'][-2:] == 'cm':
			ht = int(set['hgt'][:-2])
			if ht<150 or ht>193:
				return False
		elif set['hgt'][-2:] == 'in':
			ht = int(set['hgt'][:-2])
			if ht<59 or ht>76:
				return False
		else:
			return False
		hcl = set['hcl']
		if len(hcl)!=7 or hcl[0]!='#' or isHex(hcl[1:]) == False:
			return False
		if set['ecl'] not in ['amb','blu','brn','gry','grn','hzl','oth']:
			return False
		pid = set['pid']
		if len(pid)==9:
			int(pid)
		else:
			return False
	except:
		return False
	return True


temp={}

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		if len(content)==1:
			dat.append(temp);
			temp={}
			continue
		for pair in content.strip().split(' '):
			set = pair.split(':')
			if len(set)==2:
				temp[set[0]] = set[1]
			else:
				print(len(content))
if len(temp)>0:
	dat.append(temp)
	
nValid1 = 0
nValid2 = 0
for set in dat:
	if valid(set):
		nValid1 += 1
		if valid2(set):
			nValid2 += 1
			
print(nValid1)
print(nValid2)