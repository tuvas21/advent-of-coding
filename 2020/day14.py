fname = 'day14.txt'

dat = []

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append(content.strip())
		
d = {}
		
for l in dat:
	s = l.split(' = ')
	c = s[0]
	if c == 'mask':
		n = [a for a in s[1]]
		for i in range(len(n)):
			if n[i] == 'X':
				n[i] = '1'
		cmask0 = int(''.join(n),2)
		n = [a for a in s[1]]
		for i in range(len(n)):
			if n[i] == 'X':
				n[i] = '0'
		cmask1 = int(''.join(n),2)
	else:
		a = int(c.split('[')[1][:-1])
		n = int(s[1])
		d[a] = (n | cmask1) & cmask0
	
t = 0	
for a in d:
	t += d[a]
print(t)