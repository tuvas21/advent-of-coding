fname = 'day17.txt'

dat = []

def nn(d,x,y,z):
	a = -1 if d[z][y][x] else 0
	return a + sum([sum([sum(f[x-1:x+2]) for f in e[y-1:y+2]]) for e in d[z-1:z+2]])

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append(content.strip())
		
xc = 16
yc = 16
zc = 9

f = [[[False for x in range(xc * 2 -1)] for y in range(yc * 2 -1)] for z in range(zc * 2 -1)]
ct = [[[0 for x in range(xc * 2 -1)] for y in range(yc * 2 -1)] for z in range(zc * 2 +1)]

mx = int((len(dat) - 1) /2)
my = int((len(dat[0]) - 1) /2)
for x in range(len(dat)):
	for y in range(len(dat[0])):
		f[zc][y-my+yc][x-mx+xc] = (dat[y][x] == '#')
		
for row in f[zc][yc-3:yc+3]:
	print(''.join(['#' if a else '.' for a in row[xc-3:xc+3]]))
print()
		
for i in range(6):	
	for z in range(1,len(f)-1):
		for y in range(1,len(f[0])-1):
			for x in range(1,len(f[0][0])-1):
				ct[z][y][x] = nn(f,x,y,z)
	for z in range(len(f)):
		for y in range(len(f[0])):
			for x in range(len(f[0][0])):
				if f[z][y][x] and ct[z][y][x] not in [2,3]:
					f[z][y][x] = False
				if not f[z][y][x] and ct[z][y][x] ==3:
					f[z][y][x] = True
for row in f[zc-2][yc-4:yc+5]:
	print(''.join(['#' if a else '.' for a in row[xc-4:xc+5]]))
print(sum([sum([sum(d) for d in e]) for e in f]))
