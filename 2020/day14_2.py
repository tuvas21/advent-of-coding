fname = 'day14.txt'

dat = []

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append(content.strip())
		
d = {}
		
for l in dat:
	s = l.split(' = ')
	if s[0] == 'mask':
		r = [len(s[1]) - i - 1 for i in range(len(s[1])) if s[1][i] =='X']
		n = [a for a in s[1]]
		for i in range(len(n)):
			if n[i] == 'X':
				n[i] = '0'
			else:
				n[i] = '1'
		cmask0 = int(''.join(n),2)
		n = [a for a in s[1]]
		for i in range(len(n)):
			if n[i] == 'X':
				n[i] = '0'
		cmask1 = int(''.join(n),2)
	else:
		a = int(s[0].split('[')[1][:-1])
		a = (a | cmask1) & cmask0
		n = int(s[1])
		for i in range(pow(2,len(r))):
			s = 0
			b = bin(i)[2:].zfill(len(r))
			
			for j in range(len(b)):
				c = b[j]
				if c == '1':
					s += pow(2,r[j])
			d[a + s] = n
	
t = 0	
for a in d:
	t += d[a]
print(t)