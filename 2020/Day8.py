fname = 'Day8.txt'


dat = []
with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		sp = content.strip().split(' ')
		if sp[1][0] == '+':
			n = int(sp[1][1:])
		else:
			n = int(sp[1])
		dat.append((sp[0],n))

p = 0
a = 0
ex = set()
while p<len(dat):
	c = dat[p][0]
	n = dat[p][1]
	
	if p in ex:
		break
	ex |= set([p])
	if c == 'jmp':
		p += n
		continue
	elif c == 'acc':
		a += n

	p += 1
	
print (a)

mn = len(dat)
mx = len(dat)

sets = [(mn, mx)]

done = False

while not done and len(sets)>0:
	nSets = []
	for s in sets:
		mn = s[0]
		mx = s[1]
	for p in range(len(dat)):
		c = dat[p][0]
		n = dat[p][1]

		if c == 'jmp' and p + n >= mn and p + n <= mx:
			print(p,'jmp',n)
			t = p
			while True:
				t = t -1
				if dat[t][0] == 'jmp':
					nSets.append((t + 1,p))
					break
				#Not right yet...
				elif dat[t][0] == 'nop' and t + dat[t][1] in ex:
					print('Found nop',t)
					done = True
					break
				elif t in ex:
					#Can get here!!!!
					print('Found jmp',t)
					done = True
					break
	sets = nSets
	print (sets)
				
print(sorted(list(ex)))
#The previous bit of code was an attempt to find it in a more eligant way

cop = []
	
for p in range(len(dat)):
	c = dat[p][0]
	if c in ['jmp','nop']:
		cop += [p]
		
for change in cop:
	good = True
	p = 0
	a = 0
	ex = set()
	while p<len(dat):
		c = dat[p][0]
		n = dat[p][1]
		
		if p == change:
			if c == 'nop':
				c = 'jmp'
			elif c == 'jmp':
				c = 'nop'
			else:
				print('Bad stuff here!!!!!')
			#Hacked based on data from above. Too lazy to make it work all the time
		
		if p in ex:
			good = False
			#print('Failed!!!!')
			break
		ex |= set([p])
		if c == 'jmp':
			p += n
			continue
		elif c == 'acc':
			a += n
		p += 1
	if (good):
		print (a,change,dat[change])
	
