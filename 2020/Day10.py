fname = 'day10.txt'

dat = []

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append(int(content.strip()))
		
#dat = [16,10,15,5,1,11,7,19,6,12,4]
		
dat = sorted(dat)
#print(dat)

j1 = 1
j3 = 1

c = 1
t = 1

lookup = {0:1,1:1,2:2,3:4,4:7,5:13}

counts = {}

for i in range(1,len(dat)):
	d = dat[i]-dat[i-1]
	if d == 1:
		j1 += 1
		t += 1
	elif d == 3:
		j3 += 1
		if t not in counts:
			counts[t]=0
		counts[t]+=1
		c *= lookup[t]
		t = 0
	else:
		print('Error!')
if t not in counts:
	counts[t]=0
counts[t]+=1
c *= lookup[t]
t = 0
print(j1,j3,j1 * j3)
print(c)
for c in sorted(counts):
	print(c,counts[c])