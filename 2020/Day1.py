fname = 'Day1.txt'

dat=[]

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append(int(content.strip()))
		
dat = sorted(dat)
#print(dat)
si = 0
ei = len(dat)-1
while True:
	s = dat[si]+dat[ei]
	if s == 2020:
		print(dat[si]*dat[ei])
		break
	elif s < 2020:
		si += 1
	elif s > 2020:
		ei -= 1

summed = []
paired = {}

for i in range(0,len(dat)-1):
	a = dat[i]
	for b in dat[i+1:]:
		if a+b < 2020 - dat[0]:
			summed.append(a+b)
			paired[a+b] = a*b
		else:
			break
summed = sorted(list(set(summed)))

si = 0
ei = len(dat)-1
while True:
	s = summed[si]+dat[ei]
	if s == 2020:
		print(paired[summed[si]]*dat[ei])
		break
	elif s < 2020:
		si += 1
	elif s > 2020:
		ei -= 1