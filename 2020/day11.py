fname = 'day11.txt'

dat = []

with open(fname) as f:
	while(True):
		content = f.readline()
		if len(content)==0:
			break;
		dat.append([c for c in content.strip()])

diff=True
while diff:
	diff=False
	prev = [row[:] for row in dat[:]]
	for i in range(len(dat)):
		for j in range(len(dat[0])):
			seat = prev[i][j]
			if seat!='.':
				il = max(0,i-1)
				iu = min(i+2,len(prev))
				jl = max(0,j-1)
				ju = min(j+2,len(prev[0]))
				count = sum([sum([c == '#' for c in r[jl:ju]]) for r in prev[il:iu]])
				if seat == 'L' and count == 0:
					dat[i][j]='#'
					diff=True
				elif seat == '#' and count > 4:
					dat[i][j]='L'
					diff=True

print(sum([sum([c=='#' for c in row]) for row in dat]))
