inp="vzbxkghb"
#inp="abcdezgh"
inp="vzbxxzaa"

def noInvalidLetters(test):
	return 'i' not in test and 'o' not in test and 'l' not in test

def conseqString(test):
	for i in range(len(test)-2):
		if ord(test[i]) == ord(test[i+1])-1 and ord(test[i+1]) == ord(test[i+2])-1:
			return True
	return False
	
def hasTwoPairs(test,verbose=False):
	numPairs=0
	cantTest=False
	for i in range(len(test)-1):
		if verbose:
			print((i,test[i],test[i+1],cantTest))
		if not cantTest and ord(test[i]) == ord(test[i+1]):
			numPairs+=1
			cantTest=True
		else:
			cantTest=False
	return numPairs>=2

def isValid(test):
	return noInvalidLetters(test) and conseqString(test) and hasTwoPairs(test)

test=list(inp)
cindex=len(test)-1
while(True):
	while test[cindex] <= 'z':
		if isValid(''.join(test)):
			print (''.join(test));
			quit()
		test[len(test)-1]=chr(ord(test[len(test)-1])+1)
	test[len(test)-1]='a'
	for i in range(len(test)-2,-1,-1):
		if test[i]=='z':
			test[i]='a'
		else:
			test[i]=chr(ord(test[i])+1)
			break
#	print(''.join(test))
#print(''.join(test))
	