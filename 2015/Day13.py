import itertools

happy_pair={}

def find_happiness(person1,person2):
	return happy_pair[(person1,person2)]+happy_pair[(person2,person1)]
			
def find_happiness_sitting(sittingOrder,debug=False):
	happy=0
	ccity=""
	happies=[]
#	cname=sittingOrder[-1]
	cname=""
	for name in sittingOrder:
		if cname != "":
			happy+=find_happiness(cname,name)
			happies.append([find_happiness(cname,name)])
		cname=name
	if debug:
		print (happies)
	return happy

file = open("Day13.inp")
for row in file:
	row=row.strip()
	vals=row.split(" ")
	change=int(vals[3])
	if vals[2]=="lose":
		change=-change
	happy_pair[(vals[0],vals[10][:-1])]=change
	
#print(happy_pair)
#print(happy_pair.keys())
	
names=list(set([a[0] for a in happy_pair.keys()]))
min_happy=1000000
max_happy=-1000000

print(names)

for sittingOrder in itertools.permutations(names):
	happy=find_happiness_sitting(sittingOrder)
	if happy<min_happy:
		min_happy=happy
	if happy>max_happy:
		max_happy=happy
print(min_happy)
print(max_happy)
