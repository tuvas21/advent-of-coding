fname="Day17.inp"

fid=open("temp.txt","w")

def combos(remaining,invals,used=[]):
	vals=invals[:]
#	print((remaining,vals,used))
	ret=0
	if len(vals)==0:
		return 0
	test=vals.pop()
	numUsed=0
	ret+=combos(remaining,vals,used+[numUsed])
	if remaining>0:
		numUsed=1
		remaining-=test
		if remaining>0:
			ret+=combos(remaining,vals,used+[numUsed])
#	print((test,ret))
	if remaining==0:
		fid.write("%i\n"%sum(used+[numUsed]))
		ret+=1
	return ret


containers=[]

with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		content=content.strip()
		
		containers.append(int(content))

containers.sort()
#containers.reverse()
print(containers)
print(combos(150,containers))
#print(combos(25,[5,5,10,15,20]))
fid.close()