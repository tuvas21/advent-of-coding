from operator import mul

file = open("Day2.inp")
paper=0;
ribbon=0
for row in file:
	row=row.strip();
	side=list(map(int,row.split('x')))
	side.sort();
	print(side);
	a1=side[0]*side[1]
	a2=side[0]*side[2]
	a3=side[1]*side[2]
	paper+=(a1+a2+a3)*2+min([a1,a2,a3])
	ribbon+=side[0]*side[1]*side[2]+2*(side[0]+side[1])
print(paper)
print(ribbon)
