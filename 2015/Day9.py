import itertools

city_pairs=[]

def find_distance(city1,city2):
	for cset in city_pairs:
		if city1 in cset and city2 in cset:
			return cset[2]
			
def find_distance_set(cityOrder,debug=False):
	distance=0
	ccity=""
	distances=[]
	for city in cityOrder:
		if ccity != "":
			distance+=find_distance(ccity,city)
			distances.append([find_distance(ccity,city)])
		ccity=city
	if debug:
		print (distances)
	return distance

file = open("Day9.inp")
for row in file:
	row=row.strip()
	vals=row.split(" ")
	city_pairs.append((vals[0],vals[2],int(vals[4])))
	
cities=list(set([a[0] for a in city_pairs]+[a[1] for a in city_pairs]))
cities_visited=[]
min_dist=1000000
max_dist=0

for cityOrder in itertools.permutations(cities):
	distance=find_distance_set(cityOrder)
	if distance<min_dist:
		min_dist=distance
	if distance>max_dist:
		max_dist=distance
print(min_dist)
print(max_dist)
