from operator import mul

file = open("Day1.inp")
loc=0;
index=0;
havePrinted=False
for row in file:
	for char in row:
		index+=1;
		if char=='(':
			loc+=1;
		else:
			loc-=1;
		if loc<0 and not havePrinted:
			print(index);
			havePrinted=True
print(loc);

