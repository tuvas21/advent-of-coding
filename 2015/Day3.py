from operator import mul


file = open("Day3.inp")
housesVisited=list();
housesVisited.append((0,0))
slocx=0;
slocy=0
rlocx=0;
rlocy=0
index=0;
roboMove=False;
for row in file:
#	row="^>v<"
	for char in row:
		index+=1;
		if(roboMove):
			if char=='^':
				rlocy+=1;
			elif char=='v':
				rlocy-=1
			elif char=='<':
				rlocx+=1
			elif char=='>':
				rlocx-=1
			else:
				print("Invalid Character")
			housesVisited.append((rlocx,rlocy));
			roboMove=False;
		else:
			if char=='^':
				slocy+=1;
			elif char=='v':
				slocy-=1
			elif char=='<':
				slocx+=1
			elif char=='>':
				slocx-=1
			else:
				print("Invalid Character")
			housesVisited.append((slocx,slocy));
			roboMove=True;
#print(housesVisited)
#print(set(housesVisited))
print(len(housesVisited))
print(len(set(housesVisited)));

