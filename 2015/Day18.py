import itertools
fname="Day18.inp"

def numNeighbors(lights):
	ret=[]
	for x in range(len(lights)):
		row=[0]*len(lights)
		for y in range(len(lights)):
			loc=[]
			if x>0 and x<len(lights)-1:
				xs=[x-1,x,x+1]
			elif x==0:
				xs=[x,x+1]
			else:
				xs=[x-1,x]
			if y>0 and y<len(lights)-1:
				ys=[y-1,y,y+1]
			elif y==0:
				ys=[y,y+1]
			else:
				ys=[y-1,y]
			neighbors=[]
			if len(ys)>len(xs):
				tempv=[list(zip(xs,temp)) for temp in itertools.permutations(ys,len(xs))]
			else:
				tempv=[list(zip(temp,ys)) for temp in itertools.permutations(xs,len(ys))]

			for trow in tempv:
				for val in trow:
					if (val[0]!=x or val[1]!=y):
						neighbors.append(val)
			neighbors=list(set(neighbors))
			row[y]=sum([lights[x][y]=="#" for x,y in neighbors])
		ret.append(row)
	return ret
			
def neighborsToLights(neighbors,lights):
	for x in range(len(lights)):
		for y in range(len(lights)):
			if lights[x][y]=="#":	#On Rules
				if neighbors[x][y] not in [2,3]:
					lights[x][y]='.'
			else:			#Off Rules
				if neighbors[x][y] in [3]:
					lights[x][y]='#'
#		print(lights[x])
	return lights
						
lights=[]
						
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		content=content.strip()
		lights.append(list(content))

print(sum([sum([v=="#" for v in row]) for row in lights]))

#tlights=[".#.#.#","...##.","#....#","..#...","#.#..#","####.."]		
#lights=[list(row) for row in tlights]

for i in range(100):
#	print(i+1)

	neighbors=numNeighbors(lights)
	lights=neighborsToLights(neighbors,lights)
	lights[0][0]="#"
	lights[len(lights)-1][0]="#"
	lights[0][len(lights)-1]="#"
	lights[len(lights)-1][len(lights)-1]="#"
#	print("neighbors")
#	for row in neighbors:
#		print(row)
#	print("lights")
#	for row in lights:
#		print(row)
	
print(sum([sum([v=="#" for v in row]) for row in lights]))
	

