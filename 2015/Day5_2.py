def hasPairNotOverlapping(name):
	prevChar=''
	for nextChar in name:
		if prevChar!='' and len(name.split(prevChar+nextChar))>2:
			return True
		prevChar=nextChar
	return False

def hasSandwitchRepeat(name):
	prevChar=''
	midChar=''
	for nextChar in name:
		if nextChar==prevChar:
			return True
		prevChar=midChar
		midChar=nextChar
	return False

numNice=0;
file = open("Day5.inp")
for row in file:
	if hasPairNotOverlapping(row) and hasSandwitchRepeat(row) :
		numNice+=1;
print(numNice);