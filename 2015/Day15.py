fname="Day15.inp"

def findScore(vals):
	capacity=0
	durability=0
	flavor=0
	texture=0
	for i in range(len(vals)):
		capacity+=data[i][1]*vals[i]
		durability+=data[i][2]*vals[i]
		flavor+=data[i][3]*vals[i]
		texture+=data[i][4]*vals[i]
	if capacity<0 or durability<0 or flavor<0 or texture<0:
		return 0
	else:
		return capacity*durability*flavor*texture
	

data=[]
rtime=2503
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		content=content.strip()
		vals=content.split(" ")
#		print(vals)
		name=vals[0][:-1]
		capacity=int(vals[2][:-1])
		durability=int(vals[4][:-1])
		flavor=int(vals[6][:-1])
		texture=int(vals[8][:-1])
		calories=int(vals[10])
		data.append((name,capacity,durability,flavor,texture,calories))
#		print((name,speed,timef,timer))
#print(data)
vals=[25,25,25,25]
prevScore=[]
while findScore(vals) not in prevScore:
	prevScore.append(findScore(vals))
	for i in range(len(vals)):
		vals[i]-=1
	tscore=findScore(vals)
	pscore=[]
	useScore=[]
	for i in range(len(vals)):
		vals[i]+=1
		scoreImprovement=findScore(vals)-tscore
		pscore.append(scoreImprovement)
		if scoreImprovement>0:
			useScore.append(True)
		else:
			useScore.append(False)
		vals[i]-=1
	numChanges=sum(useScore)
	print (pscore)
	if numChanges==4:
		t=min(pscore)
		for i in range(len(vals)):
			if pscore[i]<(max(pscore)-t)*.2+t:
				useScore[i]=False
		numChanges=sum(useScore)	#Changes the lowest value only
	if numChanges==3:
		t=max(pscore)
		index=pscore.index(t)
		for i in range(len(vals)):
			vals[i]+=useScore[i]
		vals[index]+=1
	elif numChanges==2:
		for i in range(len(vals)):
			vals[i]+=useScore[i]*2
	elif numChanges==1:
		for i in range(len(vals)):
			vals[i]+=useScore[i]*4
	print(vals)			
#	break
print (prevScore)
