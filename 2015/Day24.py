import itertools

file = open("Day24.inp")
vals=[]
for row in file:
	row=row.strip()
	vals.append(int(row))
#	vals=row.split(" ")

def findQe(group):
	prod=1
	for item in group:
		prod*=item
	return prod

numItems=100
minQe=1000000000000

vals.reverse()

desiredProduct=sum(vals)//3

def findMinQe(inItems,outItems):
	global numItems,minQe
	inItemSum=sum(inItems)
	if len(inItems)>=numItems:
		return
	for item in outItems:
		if item+inItemSum<desiredProduct:
			nextIn=inItems[:]
			nextIn.append(item)
			nextOut=outItems[:]
			nextOut.remove(item)
			findMinQe(nextIn,nextOut)
		elif inItemSum+item==desiredProduct:
			if numItems>len(inItems)+1:
				print("%i %i"%(numItems,len(inItems)))
				numItems=len(inItems)+1
				minQe=findQe(inItems+[item])
				print(minQe)
			elif numItems==len(inItems)+1:
				qe=findQe([item]+inItems)
				if qe<minQe:
					minQe=qe
					print(qe)
				
findMinQe([],vals)
print(minQe)
print(numItems)