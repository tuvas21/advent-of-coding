import itertools
import string

inp="CRnCaSiRnBSiRnFArTiBPTiTiBFArPBCaSiThSiRnTiBPBPMgArCaSiRnTiMgArCaSiThCaSiRnFArRnSiRnFArTiTiBFArCaCaSiRnSiThCaCaSiRnMgArFYSiRnFYCaFArSiThCaSiThPBPTiMgArCaPRnSiAlArPBCaCaSiRnFYSiThCaRnFArArCaCaSiRnPBSiRnFArMgYCaCaCaCaSiThCaCaSiAlArCaCaSiRnPBSiAlArBCaCaCaCaSiThCaPBSiThPBPBCaSiRnFYFArSiThCaSiRnFArBCaCaSiRnFYFArSiThCaPBSiThCaSiRnPMgArRnFArPTiBCaPRnFArCaCaCaCaSiRnCaCaSiRnFYFArFArBCaSiThFArThSiThSiRnTiRnPMgArFArCaSiThCaPBCaSiRnBFArCaCaPRnCaCaPMgArSiRnFYFArCaSiThRnPBPMgAr"

switches=[]

def breakIntoElems(inp):
	eleminp=[]
	index=0
	while index<len(inp):
		if index<len(inp)-1 and inp[index+1] in string.ascii_lowercase:
			ch=inp[index:index+2]
		else:
			ch=inp[index]
		eleminp.append(ch)
		index+=len(ch)
	return eleminp

file = open("Day19.inp")
for row in file:
	row=row.strip()
	vals=row.split(" ")
	switches.append((vals[0],vals[2]))
	
uniquevals=list(set([t[0] for t in switches]))
switchdict={}
for val in uniquevals:
	switchdict[val]=[breakIntoElems(t[1]) for t in switches if t[0]== val]
	
print (uniquevals)
print(switchdict)

backDict={}
maxSwitchLength=0
for t in switches:
	backDict[t[1]]=t[0]
	if len(t[1])>maxSwitchLength:
		maxSwitchLength=len(t[1])

eleminp=breakIntoElems(inp)
print (backDict)

testOrder=[t[1] for t in switches]
testOrder.sort(key=len,reverse=True)
print(testOrder)

minLength=10000
def simplifyTestElem(current,steps=0):
	global minLength
#	print(current)
	if current=='e':
		return steps

	steps+=1
	if steps>=minLength:
		return steps
	for x in backDict:
		if x in current:
#				length=0
			newVal=current.replace(x,backDict[x],1)
			good=False
			for y in testOrder:
				if len(y)<=len(newVal) and newVal[0:len(y)]==y :
					good=True
#			if not good:
#				continue
			length=simplifyTestElem(newVal,steps)
			if length<minLength:
				minLength=length
				print (minLength)
	return minLength

print(simplifyTestElem(inp))

	
print(testElem(['e'],len(eleminp)))
	

	
