import itertools
import string

inp="CRnCaSiRnBSiRnFArTiBPTiTiBFArPBCaSiThSiRnTiBPBPMgArCaSiRnTiMgArCaSiThCaSiRnFArRnSiRnFArTiTiBFArCaCaSiRnSiThCaCaSiRnMgArFYSiRnFYCaFArSiThCaSiThPBPTiMgArCaPRnSiAlArPBCaCaSiRnFYSiThCaRnFArArCaCaSiRnPBSiRnFArMgYCaCaCaCaSiThCaCaSiAlArCaCaSiRnPBSiAlArBCaCaCaCaSiThCaPBSiThPBPBCaSiRnFYFArSiThCaSiRnFArBCaCaSiRnFYFArSiThCaPBSiThCaSiRnPMgArRnFArPTiBCaPRnFArCaCaCaCaSiRnCaCaSiRnFYFArFArBCaSiThFArThSiThSiRnTiRnPMgArFArCaSiThCaPBCaSiRnBFArCaCaPRnCaCaPMgArSiRnFYFArCaSiThRnPBPMgAr"

switches=[]

file = open("Day19.inp")
for row in file:
	row=row.strip()
	vals=row.split(" ")
	switches.append((vals[0],vals[2]))
	
uniquevals=list(set([t[0] for t in switches]))
switchdict={}
for val in uniquevals:
	switchdict[val]=list(set([t[1] for t in switches if t[0]== val]))
	
print (uniquevals)
print(switchdict)

outputs=[]
eleminp=[]
index=0
while index<len(inp):
	if inp[index+1] in string.ascii_lowercase:
		ch=inp[index:index+2]
	else:
		ch=inp[index]
	eleminp.append(ch)
	index+=len(ch)

index=0
while index<len(eleminp):
	ch=eleminp[index]
	temp=eleminp[:]
	if ch in switchdict:
		for t in switchdict[ch]:
			temp[index]=t
			outputs.append(''.join(temp))
	index+=1

print (len(set(outputs)))
	

	
