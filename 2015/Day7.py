fname="Day7.inp"

def rshift(value1,value2):
	return value1 >> value2 & 65535
	
def andf(value1,value2):
	return value1 & value2

def orf(value1,value2):
	return value1 | value2
	
def lshift(value1,value2):
	return value1 << value2 & 65535

def notf(value1,value2):
	return ~value1

def equal(value1,value2):
	return value1
	
order={}
values={}
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content:
			break
		content=content.strip()
		parsed=content.split(" ")
		assign=parsed[-1]
		opt=parsed[1]
		if parsed[0]=="NOT":
			order[assign]=(notf,parsed[1],0)
		else:
			if opt=="->":
				try:
					values[assign]=int(parsed[0])
				except ValueError as er:
					order[assign]=(equal,parsed[0],0)
			if opt=="RSHIFT":
				order[assign]=(rshift,parsed[0],int(parsed[2]))
			elif opt=="LSHIFT":
				order[assign]=(lshift,parsed[0],int(parsed[2]))
			elif opt=="AND":
				order[assign]=(andf,parsed[0],parsed[2])
			elif opt=="OR":
				order[assign]=(orf,parsed[0],parsed[2])

def getValue(register):
	try:
		return int(register)
	except ValueError as er:
		if register in values:
			return int(values[register])
		else:
			raise ValueError("No value defined yet")

while(len(order)>0):
	delindex=[]
	for key in order.keys():
		row=order[key]
		try:
			values[key]=row[0](getValue(row[1]),getValue(row[2]))
			delindex.append( key)
		except ValueError:
			pass
	for index in delindex:
		del order[index]
print(values['a'])