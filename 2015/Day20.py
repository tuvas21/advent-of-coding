import itertools
import string

inp=29000000
#inp=100
flatten_iter = itertools.chain.from_iterable
def factors(n):
    step = 2 if n%2 else 1
    return set(flatten_iter((i, n//i) 
                for i in range(1, int(n**0.5)+1) if n % i == 0))


numhouses=0
numPresents=0
presentsPerHouse=[]
presents=0
housesPerElf=[0]*1000000
while presents<inp:
	numhouses+=1
	presents=0
	for x in factors(numhouses):
		if len(housesPerElf)<x:
			housesPerElf.append(0)
		housesPerElf[x-1]+=1
		if housesPerElf[x-1]<=50:
			presents+=x*11
#	presentsPerHouse.append(presents)
	numPresents+=presents

#print(presentsPerHouse[:10])
print(numhouses)
print(numPresents)

	

	

	
