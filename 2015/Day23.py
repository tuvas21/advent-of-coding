a=1
b=0

fname="Day23.inp"

instructions=[]
rtime=2503
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		content=content.strip()
		vals=content.split(" ")
		instructions.append(vals)

print(instructions)		
exp=0
while exp<len(instructions):
	vals=instructions[exp]
	print("%i %i %i %s"%(exp,a,b,str(vals)))
	exp+=1
	if vals[0]=="hlf":
		if vals[1]=="a":
			a=a//2
		elif vals[1]=="b":
			b=b//2
	elif vals[0]=="tpl":
		if vals[1]=="a":
			a*=3
		elif vals[1]=="b":
			b*=3
	elif vals[0]=="inc":
		if vals[1]=="a":
			a+=1
		elif vals[1]=="b":
			b+=1
	elif vals[0]=="jmp":
		exp+=int(vals[1])-1
	elif vals[0]=="jie":
		jump=False
		if vals[1][0]=="a":
			jump=a%2==0
		elif vals[1][0]=="b":
			jump=b%2==0	
		if jump:
			exp+=int(vals[2])-1
	elif vals[0]=="jio":
		jump=False
		if vals[1][0]=="a":
			jump=a==1
		elif vals[1][0]=="b":
			jump=b==1
		if jump:
			exp+=int(vals[2])-1

print(exp)
print(a)
print(b)			
		