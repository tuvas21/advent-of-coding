import itertools
import string

inp="CRnCaSiRnBSiRnFArTiBPTiTiBFArPBCaSiThSiRnTiBPBPMgArCaSiRnTiMgArCaSiThCaSiRnFArRnSiRnFArTiTiBFArCaCaSiRnSiThCaCaSiRnMgArFYSiRnFYCaFArSiThCaSiThPBPTiMgArCaPRnSiAlArPBCaCaSiRnFYSiThCaRnFArArCaCaSiRnPBSiRnFArMgYCaCaCaCaSiThCaCaSiAlArCaCaSiRnPBSiAlArBCaCaCaCaSiThCaPBSiThPBPBCaSiRnFYFArSiThCaSiRnFArBCaCaSiRnFYFArSiThCaPBSiThCaSiRnPMgArRnFArPTiBCaPRnFArCaCaCaCaSiRnCaCaSiRnFYFArFArBCaSiThFArThSiThSiRnTiRnPMgArFArCaSiThCaPBCaSiRnBFArCaCaPRnCaCaPMgArSiRnFYFArCaSiThRnPBPMgAr"

switches=[]

def breakIntoElems(inp):
	eleminp=[]
	index=0
	while index<len(inp):
		if index<len(inp)-1 and inp[index+1] in string.ascii_lowercase:
			ch=inp[index:index+2]
		else:
			ch=inp[index]
		eleminp.append(ch)
		index+=len(ch)
	return eleminp
	
file = open("Day19.inp")
for row in file:
	row=row.strip()
	vals=row.split(" ")
	switches.append((vals[0],vals[2]))
	
uniquevals=list(set([t[0] for t in switches]))
switchdict={}
for val in uniquevals:
	switchdict[val]=[breakIntoElems(t[1]) for t in switches if t[0]== val]

backDict={}
maxSwitchLength=0
for t in switches:
	backDict[t[1]]=t[0]
	if len(t[1])>maxSwitchLength:
		maxSwitchLength=len(t[1])

eleminp=breakIntoElems(inp)

testOrder=[t[1] for t in switches]
testOrder.sort(key=len,reverse=True)

subtractStartFactors={"CRnSi":10,"CRnCa":8,"CRnP":8}

def estLookAhead(inp,elems):
	subFactor=0
	subFactor=2*inp.count("Rn")
	subFactor+=2*inp.count("Y")
#	print((elems,subFactor))
#	print (inp.count("Rn"))
#	print(inp.count("Y"))
	return elems-1-subFactor
	
#print(estLookAhead(inp,len(eleminp)))
#quit()

#Will only return down to a single element
def simplifyTestElem(current,steps=0):
	if len(breakIntoElems(current))==1:
		return current,steps
#	print(current)

	steps+=1
	for x in backDict:
		if x in current:
#				length=0
			newVal=current.replace(x,backDict[x],1)
			good=False
			for y in testOrder:
				newVal=current.replace(x,backDict[x],1)
				ret= simplifyTestElem(newVal,steps)
				if len(breakIntoElems(current))==1:
#					print((ret,steps))
					return ret,steps

	return None,None

tested=[]
possibleStrings=[(inp,0,len(inp))]
def addToPossibleStrings(newString,steps):
	elems=breakIntoElems(newString)
	count=0
	index=0
	startg=None
	while index<len(elems):
		x=elems[index]
#		print((count,startg,x,index))
		if x=="Rn":
			count+=1
			startg=index+1
		elif x=="Y":
#			print((index,count))
			if startg :
				print(elems[startg:index])
				#Must be simplified to a single value here!
				simplify,step_increase=simplifyTestElem("".join(elems[startg:index]))
				if simplify:
					print((simplify,step_increase))
					selems=breakIntoElems(simplify)
					for i in range(index-startg):
						elems.pop(index)
					elems.insert(index,selems[0])
					newString="".join(elems)
					continue;
				startg=index+1
			else:
				startg=None
		elif x=="Ar":
			count-=1
			if startg :
#				print(index)
				print(elems[startg:index])
				#Must be simplified to a single value here!
				simplify,step_increase=simplifyTestElem("".join(elems[startg:index-1]))
				if simplify:
					print((simplify,step_increase))
					selems=breakIntoElems(simplify)
					for i in range(index-startg):
						elems.pop(index)
					elems.insert(index,selems[0])
					newString="".join(elems)
					continue;
			startg=None

		if count<0:
			return
		index+=1
	elems=len(elems)
	if newString in tested:
		return
	tested.append(newString)
	for i in range(len(possibleStrings)):
		if elems+steps<possibleStrings[i][2]+steps:
#			print((i,newString,steps,estTime,elems))
			possibleStrings.insert(i,(newString,steps,elems))
			return
#	print(("end",newString,steps,estTime,elems))
	possibleStrings.append((newString,steps,elems))

#print(len(breakIntoElems("CRnSi")))
#print(estLookAhead(inp))
	
steps=0
current=inp
while current!='e':
	current,steps,blah=possibleStrings.pop(0)

	steps+=1
	
	for x in testOrder:
		if x in current:
#			print(x)
			newVal=current.replace(x,backDict[x],1)
			addToPossibleStrings(newVal,steps)
	print(possibleStrings[0])


print(steps)
	

	
