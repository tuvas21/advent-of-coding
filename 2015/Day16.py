fname="Day16.inp"

truth={"children":3,"cats":7,"samoyeds": 2,"pomeranians": 3,"akitas": 0,"vizslas": 0,"goldfish": 5,"trees": 3,"cars": 2,"perfumes": 1}

def test(type,val):
	if type in ["cats","trees"]:
		return truth[type]<val 
	elif type in ["pomeranians","goldfish"]:
		return truth[type]>val
	else:
		return truth[type]==val

with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		content=content.strip()
		
		vals=content.split(" ")
#		print(vals)
		type1=vals[2][:-1]
		type2=vals[4][:-1]
		type3=vals[6][:-1]
		val1=int(vals[3][:-1])
		val2=int(vals[5][:-1])
		val3=int(vals[7])
		if test(type1,val1) and test(type2,val2)  and test(type3,val3):
			print(vals)