import itertools

target = [2981, 3075]

def find_code_number(set):
	row_start=sum(range(1,set[0]))+1
	colvalue=sum(range(1,set[1]))+(set[0])*(set[1]-1)
	return row_start+colvalue

def find_code(target):
	num=20151125
	for i in range((find_code_number(target)-1)%33554393):
		num=(num*252533)%33554393
	return num

print(find_code(target))