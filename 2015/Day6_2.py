def Toggle(val):
	return val+2
def On(val):
	return val+1
def Off(val):
	if val>=1:
		return val-1
	else:
		return 0

lights=[[0]*1000 for i in range(1000)]
	
file = open("Day6.inp")
for row in file:
	row=row.strip()
	vals=row.split(" ")
	if len(vals)==4:
		command=Toggle
		start=list(map(int,vals[1].split(',')))
		end=list(map(int,vals[3].split(',')))
	else:
		if vals[1]=='off':
			command=Off
		else:
			command=On
		start=list(map(int,vals[2].split(',')))
		end=list(map(int,vals[4].split(',')))
	for rowi in range(start[0],end[0]+1):
		lights[rowi][start[1]:(end[1]+1)]=list(map(command,lights[rowi][start[1]:(end[1]+1)]))
num=0
for row in lights:
	for light in row:
		num+=light
print(num)
		