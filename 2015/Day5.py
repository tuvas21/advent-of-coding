from collections import Counter

def hasThreeVowels(name):
	temp=Counter(name)
	if sum([temp['a'],temp['e'],temp['i'],temp['o'],temp['u']]) >=3:
		return True
	else:
		return False
		
def hasDoubleLetter(name):
	prevChar=''
	for nextChar in name:
		if nextChar==prevChar:
			return True
		prevChar=nextChar
	return False
	
def badChar(name):
	return 'ab' in name or 'cd' in name or 'pq' in name or 'xy' in name 

numNice=0;
file = open("Day5.inp")
for row in file:
	if hasThreeVowels(row) and hasDoubleLetter(row) and not badChar(row):
		numNice+=1;
print(numNice);