fname="Day14.inp"

data={}
rtime=2503
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		content=content.strip()
		vals=content.split(" ")
		name=vals[0]
		speed=int(vals[3])
		timef=int(vals[6])
		timer=int(vals[13])
		data[name]=(speed,timef,timer)
#		print((name,speed,timef,timer))
		
		
def doRace(rtime):
	racetimes={}
	for name in data:
		time=0
		dist=0
		speed=data[name][0]
		timef=data[name][1]
		timer=data[name][2]
		while time+timer+timef<rtime:
			dist+=timef*speed
			time+=timef+timer
#			print((name,time,dist))
#		print(time+timef-rtime)
		if time+timef>rtime:
			dist+=(rtime-time)*speed
		else:
			dist+=(timef*speed)
		racetimes[name]=dist
	winner=[]
	maxDist=0
#	print(racetimes)
	for name in racetimes:
		if racetimes[name]>maxDist:
			maxDist=racetimes[name]
			winner=[name]
		elif racetimes[name]==maxDist:
			winner.append(name)
			print((name,maxDist))
	return winner
	
#data={"Comet":(14,10,127),"Dancer":(16,11,162)}
#rtime=1000
winnerStat={}
for name in data:
	winnerStat[name]=0
for i in range(1,rtime+1):
	for winner in doRace(i):
		winnerStat[winner]+=1
print(winnerStat)
