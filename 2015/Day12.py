import json

def isArray(var):
    return isinstance(var, (list, tuple))

def sumOfNumbers(inp):
	ret=0
#	print(inp)
	if isinstance(inp,str):
		try:
			return int(inp)
		except:
			return 0
	elif isinstance(inp, dict):
		for key in inp:
			if inp[key]=="red":
				return 0
			else:
				ret+=sumOfNumbers(inp[key])
	elif isArray(inp):
		for item in inp:
			ret+=sumOfNumbers(item)
	else:
		try:
			return(int(inp))
		except:
			return 0
	return ret
#		print(inp[key])

with open('Day12.inp') as data_file:    
    data = json.load(data_file)
	
print(sumOfNumbers(data))