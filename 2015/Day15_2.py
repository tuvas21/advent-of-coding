fname="Day15.inp"

def findScore(vals):
	capacity=0
	durability=0
	flavor=0
	texture=0
	for i in range(len(vals)):
		capacity+=data[i][1]*vals[i]
		durability+=data[i][2]*vals[i]
		flavor+=data[i][3]*vals[i]
		texture+=data[i][4]*vals[i]
	if capacity<0 or durability<0 or flavor<0 or texture<0:
		return 0
	else:
		return capacity*durability*flavor*texture
	

data=[]
rtime=2503
with open(fname) as f:
	while(True):
		content = f.readline()
		if not content or content=="":
			break
		content=content.strip()
		vals=content.split(" ")
#		print(vals)
		name=vals[0][:-1]
		capacity=int(vals[2][:-1])
		durability=int(vals[4][:-1])
		flavor=int(vals[6][:-1])
		texture=int(vals[8][:-1])
		calories=int(vals[10])
		data.append((name,capacity,durability,flavor,texture,calories))

maxGoodness=0
for i1 in range(101):
	for i2 in range(0,101-i1):
		for i3 in range(0,101-i1-i2):
			i4=100-i1-i2-i3
			if data[0][5]*i1+data[1][5]*i2+data[2][5]*i3+data[3][5]*i4==500:
				if maxGoodness<findScore([i1,i2,i3,i4]):
					maxGoodness=findScore([i1,i2,i3,i4])
					
print(maxGoodness)
